(ns dev
  (:require [clojure.repl :refer :all]
            [clojure.spec.alpha :as s]
            [orchestra.spec.test :as st]
            [juxt.clip.repl :as clipr :refer [start stop reset system]]
            [expound.alpha :as expound]
            [idle.main :as main]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(clipr/set-init! #(main/system-config :dev))
