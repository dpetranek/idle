(ns idle.masks.views.home)

(defn masks-page []
  [:div [:h2 "Masks"]
   [:select
    {:on-change (fn [e] (println (.-value (.-target e))))}
    [:option {:value :beacon} "Beacon"]
    [:option {:value :bull} "Bull"]
    [:option {:value :delinquent} "Delinquent"]
    [:option {:value :doomed} "Doomed"]
    [:option {:value :janus} "Janus"]
    [:option {:value :legacy} "Legacy"]
    [:option {:value :nova} "Nova"]
    [:option {:value :outsider} "Outsider"]
    [:option {:value :protege} "Protégé"]
    [:option {:value :transformed} "Transformed"]]])
