(ns idle.masks.data
  (:require [datahike.api :as d]
            [idle.services.datahike :as db]))

(def playbook-schema
  [
   {:db/ident       :playbook/id
    :db/valueType   :db.type/keyword
    :db/unique      :db.unique/identity
    :db/cardinality :db.cardinality/one}

   {:db/ident       :playbook/name
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one}

   {:db/ident       :playbook/influence
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one}

   {:db/ident       :playbook/moment-of-truth
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one}

   {:db/ident       :playbook/ability
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one}

   {:db/ident       :playbook/abilities
    :db/valueType   :db.type/ref
    :db/cardinality :db.cardinality/many}

   {:db/ident       :playbook/moves
    :db/valueType   :db.type/ref
    :db/cardinality :db.cardinality/many}

   {:db/ident       :playbook/prompts
    :db/valueType   :db.type/ref
    :db/cardinality :db.cardinality/many}

   {:db/ident       :playbook/extras
    :db/valueType   :db.type/ref
    :db/cardinality :db.cardinality/many}

   {:db/ident       :playbook/advances
    :db/valueType   :db.type/ref
    :db/cardinality :db.cardinality/many}])

(def playbook-beacon
  [{:playbook/id :beacon}
   {:playbook/id              :beacon
    :playbook/name            "The Beacon"
    :playbook/influence       "You are so excited to be here. Give Influence over you to three of your teammates."
    :playbook/moment-of-truth "This is the moment when you show them exactly why you belong here. You do any one thing, take out any one enemy, no matter how insane, no matter how ridiculous, because that’s you. Their jaws are gonna drop when you’re done. Of course, pulling off a stunt like this tends to bring unwanted attention and a dangerous reputation..."
    :playbook/ability         "If you have superpowers, they’re pretty minor or not noticeable. If you have skills, you carry the necessary equipment. Choose two."
    :playbook/abilities
    [{:ability/description "bow and trick arrows"
      :ability/playbook    [:playbook/id :beacon]}
     {:ability/description "camouflage and stealth"
      :ability/playbook    [:playbook/id :beacon]}
     {:ability/description "phasing"
      :ability/playbook    [:playbook/id :beacon]}
     {:ability/description "swords"
      :ability/playbook    [:playbook/id :beacon]}
     {:ability/description "martial arts"
      :ability/playbook    [:playbook/id :beacon]}
     {:ability/description "acrobatics"
      :ability/playbook    [:playbook/id :beacon]}]
    :playbook/prompts
    [{:prompt/q        "How did you gain your skills?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :beacon]}
     {:prompt/q        "When did you first put on your costume?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :beacon]}
     {:prompt/q        "Who, outside of the team, thinks you shouldn’t be a superhero?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :beacon]}
     {:prompt/q        "Why do you try to be a hero?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :beacon]}
     {:prompt/q        "Why do you care about the team?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :beacon]}
     {:prompt/q        "We found signs that this incident was just the start of something bigger. What were the signs?"
      :prompt/type     :prompt/team
      :prompt/playbook [:playbook/id :beacon]}
     {:prompt/q        "___ is awesome, and you take every chance you get to hang out with them."
      :prompt/type     :prompt/relationship
      :prompt/playbook [:playbook/id :beacon]}
     {:prompt/q        "You've got to prove yourself to ___ before you feel like a real hero."
      :prompt/type     :prompt/relationship
      :prompt/playbook [:playbook/id :beacon]}]
    :playbook/advances
    [{:advance/type        :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook    [:playbook/id :beacon]}

     {:advance/type        :advance/major
      :advance/description "Take an adult move"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/major
      :advance/description "Take an adult move"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/major
      :advance/description "Unlock your Moment of Truth after it's been used once"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/major
      :advance/description "Change playbooks"
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook    [:playbook/id :beacon]}
     {:advance/type        :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook    [:playbook/id :beacon]}]
    :playbook/moves
    [{:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :beacon]
      :move/name        "Straight. Up. Creepin'."
      :move/description "When you scope out a person or place, roll + Mundane. On a 10+, ask two. On a 7-9, ask one. - what’s my best way in/out? - what happened here recently? - what here is worth grabbing? - who or what here is not what they seem? - whose place is this? On a miss, you find yourself in over your head. The GM will tell you why this is a bad spot."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :beacon]
      :move/name        "No powers and not nearly enough training"
      :move/description "You’re always picking up new gear to keep yourself in the game. Whenever you pick up a new piece of gear, you can write it in as a new ability if this line is empty. The first time you use each piece of gear to directly engage a threat, unleash your powers, or defend someone, you can roll + Mundane instead of the normal Label."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :beacon]
      :move/name        "Won't let you down"
      :move/description "When you help a teammate, you can spend 2 out of the Team pool to add +2 to their roll."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :beacon]
      :move/name        "Pretty much a superhero"
      :move/description "When you bring up your superhero name to someone important (your call) for the first time, roll + Savior. On a hit, they’ve heard of you; say which of your exploits they’ve heard about and which Label they think applies. On a 7-9, the GM will tell you something else they’ve heard, and pick a second Label they assign to you. On a miss, they don’t take you seriously or mistrust you moving forward."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :beacon]
      :move/name        "C'mon, Lucky"
      :move/description "You have a pet of some kind, a smaller companion that helps you out. Detail it. Choose three basic moves and tell the GM how it helps you with those moves. Whenever your pet could help you, take +1 to that move. If your pet ever gets hurt, treat it as taking a powerful blow."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :beacon]
      :move/name        "Suck it, Domitian"
      :move/description "When you stand strong while dramatically under fire, roll + Savior instead of + Danger to directly engage a threat."}]
    :playbook/extras
    [{:extra/playbook    [:playbook/id :beacon]
      :extra/name        "Drives"
      :extra/description "Choose four drives to mark at the start of play. When you fulfill a marked drive, strike it out, and choose one: mark potential, clear a condition, take Influence over someone involved. When your four marked drives are all struck out, choose and mark four new drives. When all drives are struck out, change playbooks, retire from the life, or become a paragon of the city."
      :extra/options     (mapv (fn [%] {:drive/extra [:extra/name "Drives"] :drive/description %}) ["lead the team successfully in battle" "kiss someone dangerous" "punch someone you probably shouldn’t" "help a teammate when they most need you" "take down a threat all on your own" "outperform an adult hero" "pull off a ridiculous stunt save a teammate’s life" "get drunk or high with a teammate" "drive a fantastical vehicle" "get a new costume" "get a new hero name" "earn the respect of a hero you admire" "make out with a teammate" "punch out a teammate" "break up with someone" "stop a fight" "with calm words tell someone your true feelings for them" "travel to an incredible place (or time)" "reject someone who tells you “you shouldn’t be here”"])}]}])

(def playbook-bull
  [{:playbook/id :bull}
   {:db/id                    [:playbook/id :bull]
    :playbook/name            "The Bull"
    :playbook/influence       "You’re selective about who you let in. Give your love and rival Influence over you, but that’s it."
    :playbook/moment-of-truth "This is what you do best. You let loose, all the pent up strength and rage and glee, and you break whatever stands in your way. You are a walking demolition crew. What can stand up to you? Nothing. Not buildings. Not structures. Not enemies. Nothing. Of course, now the people who changed you know exactly where to find you..."
    :playbook/ability         "Someone or something changed you, made you into a perfect weapon: superhumanly tough, incredibly strong, and uniquely skilled at fighting. Decide how each of those abilities manifests."
    :playbook/prompts
    [{:prompt/q        "Who changed you?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :bull]}
     {:prompt/q        "How did you escape from them?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :bull]}
     {:prompt/q        "Who, outside the team, tries to take care of you now?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :bull]}
     {:prompt/q        "Why do you try to be a hero?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :bull]}
     {:prompt/q        "Why do you care about the team?"
      :prompt/type     :prompt/individual
      :prompt/playbook [:playbook/id :bull]}
     {:prompt/q        "We defeated a dangerous enemy. Who or what was it?"
      :prompt/type     :prompt/team
      :prompt/playbook [:playbook/id :bull]}
     {:prompt/q        "___ is your love. You've opened up to them aobut the worst parts of your past."
      :prompt/type     :prompt/relationship
      :prompt/playbook [:playbook/id :bull]}
     {:prompt/q        "___ is your rival. They tried to control you at a crucial moment."
      :prompt/type     :prompt/relationship
      :prompt/playbook [:playbook/id :bull]}]
    :playbook/advances
    [{:advance/type        :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/minor
      :advance/description "Choose another two roles for The Bull's Heart"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/major
      :advance/description "Unlock your Moment of Truth after it's been used once"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/major
      :advance/description "Change playbooks"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/major
      :advance/description "Take an adult move"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/major
      :advance/description "Take an adult move"
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook    [:playbook/id :bull]}
     {:advance/type        :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook    [:playbook/id :bull]}]
    :playbook/moves
    [{:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :bull]
      :move/name        "Thick and thin skinned"
      :move/description "Whenever you have Angry marked, take +1 ongoing to unleash your powers."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :bull]
      :move/name        "You’ve got a head you don’t need"
      :move/description "When you provoke someone with obvious threats and shows of force, roll + Danger instead of + Superior."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :bull]
      :move/name        "Punch everyone"
      :move/description "Whenever you charge into a fight without hedging your bets, you can shift your Danger up and any other Label down."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :bull]
      :move/name        "There when it matters"
      :move/description "When you defend someone, on a hit you can hold 1 instead of choosing one from the list. Spend your hold when they are in danger later to arrive on the scene ready to help."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :bull]
      :move/name        "In a china shop"
      :move/description "When you directly engage a threat, you can cause significant collateral damage to your environment to choose an additional option, even on a miss."}
     {:move/type        :move/playbook-move
      :move/playbook    [:playbook/id :bull]
      :move/name        "Physics? What physics?"
      :move/description "When you unleash your powers to barrel through an insurmountable barrier, roll + Danger instead of + Freak."}]
    :playbook/extras
    [{:extra/playbook    [:playbook/id :bull]
      :extra/id          :bulls-heart
      :extra/name        "The Bull's Heart"
      :extra/description "You always have exactly one love and one rival. You can change your love or rival at any time; give the new subject of your affections or disdain Influence over you. Take +1 ongoing to any action that impresses your love or frustrates your rival. Choose a role you commonly fulfill for your love or rival."
      :extra/options
      [{:heart-role/extra       [:extra/name "The Bull's Heart"]
        :heart-role/name        "Defender"
        :heart-role/description "When you leap to defend your love or rival in battle, roll + Danger instead of + Savior to defend them."}
       {:heart-role/extra       [:extra/name "The Bull's Heart"]
        :heart-role/name        "Friend"
        :heart-role/description "When you comfort or support your love or rival, mark potential on a hit. When your love or rival comforts or supports you, mark potential when they roll a hit."}
       {:heart-role/extra       [:extra/name "The Bull's Heart"]
        :heart-role/name        "Listener"
        :heart-role/description "When you pierce the mask of your love or rival, you can always let them ask you a question to ask them an additional question in turn, even on a miss. These additional questions do not have to be on the list."}
       {:heart-role/extra       [:extra/name "The Bull's Heart"]
        :heart-role/name        "Enabler"
        :heart-role/description "When you provoke your love or rival, roll + Danger if you are trying to provoke them to rash or poorly thought out action."}]}]
    }])

(def playbook-delinquent
  [{:playbook/id :delinquent}
   {:playbook/id :delinquent
    :playbook/name "The Delinquent"
    :playbook/influence "You care way more than you let on. Give three teammates Influence over you."
    :playbook/moment-of-truth "This is when you show them what you really are. Whether you’re the hero underneath the rebel facade... or the one who can make the hard choices heroes can’t make. You do whatever it takes to show that truth, whether it’s saving the day from a terrible villain or stopping a bad guy once and for all. Of course, once you’ve shown what you really are, there’s no going back to playing the clown..."
    :playbook/ability "Your powers are messy, deceiving, or frustrating. Choose two."
    :playbook/abilities
    [{:ability/description "teleportation"
      :ability/playbook [:playbook/id :delinquent]}
     {:ability/description "gadgetry and hacking"
      :ability/playbook [:playbook/id :delinquent]}
     {:ability/description "emotion control"
      :ability/playbook [:playbook/id :delinquent]}
     {:ability/description "tricks/illusions"
      :ability/playbook [:playbook/id :delinquent]}
     {:ability/description "psychic weapons"
      :ability/playbook [:playbook/id :delinquent]}
     {:ability/description "power negation"
      :ability/playbook [:playbook/id :delinquent]}]
    :playbook/prompts
    [{:prompt/q "How did you get your powers?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :delinquent]}
     {:prompt/q "What do you do for fun?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :delinquent]}
     {:prompt/q "Who, outside the team, thinks better of you than you do?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :delinquent]}
     {:prompt/q "Why do you try to be a hero?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :delinquent]}
     {:prompt/q "Why do you care about the team?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :delinquent]}
     {:prompt/q "We totally broke some major rules to win the fight. What rules did we break? Whose rules were they?"
      :prompt/type :prompt/team
      :prompt/playbook [:playbook/id :delinquent]}
     {:prompt/q "You keep trying to impress ___ with your antics."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :delinquent]}
     {:prompt/q "You and ___ pulled an awesome (if illegal) stunt together."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :delinquent]}]
    :playbook/advances
    [{:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/minor
      :advance/description "Add +1 to any two Labels"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/major
      :advance/description "Unlock your Moment of Truth after it's been used once"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/major
      :advance/description "Change playbooks"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook [:playbook/id :delinquent]}
     {:advance/type :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook [:playbook/id :delinquent]}]
    :playbook/moves
    [{:move/type :move/playbook-move
      :move/playbook [:playbook/id :delinquent]
      :move/name "Mary Contrary"
      :move/description "When someone tries to pierce your mask, comfort or support you, or provoke you, you can interfere. Roll + Superior. On a hit, they take a -2 on their roll. On a 10+, you also take Influence over them or clear a condition. On a miss, they get a 10+ no matter what they rolled and you mark a condition of their choice."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :delinquent]
      :move/name "I don't care what you think!"
      :move/description "Whenever you reject others’ Influence, add +2 to your roll."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :delinquent]
      :move/name "Team? What team?"
      :move/description "When you use Team selfishly, clear a condition or mark potential. The first time in a session that you use Team to help a teammate, take +1 forward."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :delinquent]
      :move/name "Criminal mind"
      :move/description "When you assess the situation, you can always ask one of the following questions, even on a miss: - what here is useful or valuable to me?  - how could I best infuriate or provoke ___?  - what’s the best way in/way past?"}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :delinquent]
      :move/name "Troublemaker"
      :move/description "When you help a teammate through destructive, criminal, or rule-breaking actions, you can give them a +2 instead of a +1 when you spend a Team from the pool."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :delinquent]
      :move/name "Are you watching closely?"
      :move/description "When you mislead, distract, or trick someone, roll + Superior. On a hit, they are fooled, at least for a moment. On a 10+, choose three. On a 7-9, choose two. - you get an opportunity - you expose a weakness or flaw - you confuse them for some time - you avoid further entanglement On a miss, you’re hopelessly embroiled in it and under pressure; mark a condition."}]
    }])

(def playbook-doomed
  [{:playbook/id :doomed}
   {:playbook/id :doomed
    :playbook/name "The Doomed"
    :playbook/influence "These people matter for what you need to do. Give Influence to two of your teammates."
    :playbook/moment-of-truth "The prickly tingling fear of your doom, always in your head—it holds you back most of the time. But right here, right now? It gives you the confidence to do anything. After all, what’s the worst that could happen? Is it worse than your doom? Do impossible things. Do anything. But mark a doomsign after you’re finished."
    :playbook/ability "Your powers are tied into your doom; think about the nature of your doom when you choose them. Choose up to three."
    :playbook/abilities
    [{:ability/description "telekinesis"
      :ability/playbook [:playbook/id :doomed]}
     {:ability/description "memory manipulation"
      :ability/playbook [:playbook/id :doomed]}
     {:ability/description "psychic constructs"
      :ability/playbook [:playbook/id :doomed]}
     {:ability/description "body transmutation"
      :ability/playbook [:playbook/id :doomed]}
     {:ability/description "superhuman strength and speed"
      :ability/playbook [:playbook/id :doomed]}
     {:ability/description "vitality absorption"
      :ability/playbook [:playbook/id :doomed]}]
    :playbook/prompts
    [{:prompt/q "When did you first learn of your doom?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :doomed]}
     {:prompt/q "Where did you get your sanctuary?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :doomed]}
     {:prompt/q "Why do you oppose your nemesis?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :doomed]}
     {:prompt/q "Who, outside of the team, is crucial to defeating your nemesis?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :doomed]}
     {:prompt/q "Why does the team matter to you?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :doomed]}
     {:prompt/q "We paid a high cost for victory. What was it?"
      :prompt/type :prompt/team
      :prompt/playbook [:playbook/id :doomed]}
     {:prompt/q "You told ___ all about your doom and the danger you're in."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :doomed]}
     {:prompt/q "You'd love to kiss ___ before your doom comes."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :doomed]}]
    :playbook/advances
    [{:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/minor
      :advance/description "Clear a doomsign; you lose access to that move for now"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/minor
      :advance/description "Get burn and three flares (from the Nova’s playbook)"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/major
      :advance/description "Unlock your Moment of Truth after it's been used once"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/major
      :advance/description "Change playbooks"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook [:playbook/id :doomed]}
     {:advance/type :advance/major
      :advance/description "Confront your doom on your terms; if you survive, change playbooks"
      :advance/playbook [:playbook/id :doomed]}]
    :playbook/extras
    [{:extra/playbook [:playbook/id :doomed]
      :extra/id :nemesis
      :extra/name "Nemesis"
      :extra/description "You have a nemesis, an epic and powerful enemy representing and embodying your doom. It’s going to take everything you have to take them down in the time you have left. At the end of every session, answer the question: Did you make progress on defeating your nemesis? If the answer is yes, mark potential. If the answer is no, mark your doom track. Who is your nemesis?"}
     {:extra/playbook [:playbook/id :doomed]
      :extra/id :doom
      :extra/name "Doom"
      :extra/description "You’re doomed. Your powers may be killing you, or maybe you’re hunted ruthlessly, or maybe you embody an apocalypse. But one way or another, your future is grim. What brings your doom closer? Choose two. Whenever you bring your doom closer, mark one box on your doom track. When your doom track fills, clear it and take one of your doomsigns."
      :extra/options
      [{:doombringer/extra [:extra/name "Doom"]
        :doombringer/description "overexerting yourself"}
       {:doombringer/extra [:extra/name "Doom"]
        :doombringer/description "injuring innocents"}
       {:doombringer/extra [:extra/name "Doom"]
        :doombringer/description "facing danger alone"}
       {:doombringer/extra [:extra/name "Doom"]
        :doombringer/description "frightening loved ones"}
       {:doombringer/extra [:extra/name "Doom"]
        :doombringer/description "showing mercy"}
       {:doombringer/extra [:extra/name "Doom"]
        :doombringer/description "talking about it openly"}]}
     {:extra/playbook [:playbook/id :doomed]
      :extra/id :doomsigns
      :extra/name "Doomsigns"
      :extra/description "These are abilities that come to you with your approaching doom. Once you have taken all five doomsigns above the line, you must take “Your doom arrives” the next time your doom track fills. Choose one doomsign you already hold at character creation."
      :extra/options
      [{:doomsign/extra [:extra/name "Doomsigns"]
        :doomsign/description "Dark Visions: Mark your doom track to have a vision about the situation at hand. After the vision, ask the GM a question; they will answer it honestly."}
       {:doomsign/extra [:extra/name "Doomsigns"]
        :doomsign/description "Infinite Powers: Mark your doom track to use an ability from any playbook, one time."}
       {:doomsign/extra [:extra/name "Doomsigns"]
        :doomsign/description "Portal: Mark your doom track to appear in a scene with anyone you want."}
       {:doomsign/extra [:extra/name "Doomsigns"]
        :doomsign/description "Burning Bright: Mark your doom track to ignore one of the GM’s stated requirements when you call upon the resources of your sanctuary."}
       {:doomsign/extra [:extra/name "Doomsigns"]
        :doomsign/description "Bolstered: Mark your doom track to use an Adult Move one time."}
       {:doomsign/extra [:extra/name "Doomsigns"]
        :doomsign/description "Your doom arrives; confront it and perish."}]}
     {:extra/playbook [:playbook/id :doomed]
      :extra/id :sanctuary
      :extra/name "Sanctuary"
      :extra/description "You have a place where you can rest, recover, and reflect upon your powers. Choose and underline three features of your sanctuary: An aide or assistant; locks and traps; a library of valuable tomes; a scattering of ancient relics; a teleportal; a containment system; a powerful computer; useful tools; a meditation space; a power battery; a power enhancement system; healing equipment; art, music, and food Choose and underline two downsides of your sanctuary: Difficult to access, draws dangerous attention, location known to many, easily damaged or tampered with, tied intricately to your doom When you call upon the resources of your sanctuary to solve a problem, say what you want to do. The GM will give you one to four conditions you must fulfill to complete your solution:"
      :extra/options
      [{:sanctuary-prereq/extra [:extra/name "Sanctuary"]
        :sanctuary-prereq/description "first, you must ___"}
       {:sanctuary-prereq/extra [:extra/name "Sanctuary"]
        :sanctuary-prereq/description "you'll need help from  ___"}
       {:sanctuary-prereq/extra [:extra/name "Sanctuary"]
        :sanctuary-prereq/description "you and your team will risk danger from  ___"}
       {:sanctuary-prereq/extra [:extra/name "Sanctuary"]
        :sanctuary-prereq/description "the best you can do is a lesser versoin, unreliable and limited"}
       {:sanctuary-prereq/extra [:extra/name "Sanctuary"]
        :sanctuary-prereq/description "you'll need to mark one box from your doom track"}
       {:sanctuary-prereq/extra [:extra/name "Sanctuary"]
        :sanctuary-prereq/description "you'll have to obtain ___"}]}
     ]}])

(def playbook-janus
  [{:playbook/id :janus}
   {:playbook/id :janus
    :playbook/name "The Janus"
    :playbook/influence "You look up to your teammates; they seem to have this superhero thing figured out. Give two of them Influence over you."
    :playbook/moment-of-truth "The mask is a lie, and some piece of you has always known that. Doesn’t matter if others can see it. You’re the one that can do the impossible. Mask off. Costume on. And you’re going to save the damn day. Of course, you better hope nobody nasty is watching…"
    :playbook/ability "Your appearance is unchanged by your abilities, and you can keep your powers hidden. You have heightened physical abilities (strength, agility, toughness), and choose two unique abilities:"
    :playbook/abilities
    [{:ability/description "rodent/insect control"
      :ability/playbook [:playbook/id :janus]}
     {:ability/description "energy absorption"
      :ability/playbook [:playbook/id :janus]}
     {:ability/description "impossible mobility"
      :ability/playbook [:playbook/id :janus]}
     {:ability/description "bone generation, venom, or webs"
      :ability/playbook [:playbook/id :janus]}
     {:ability/description "supernatural senses"
      :ability/playbook [:playbook/id :janus]}
     {:ability/description "substance mimicry"
      :ability/playbook [:playbook/id :janus]}]
    :playbook/prompts
    [{:prompt/q "When did you first put on the mask? Why?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :janus]}
     {:prompt/q "Why do you keep a secret identity?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :janus]}
     {:prompt/q "Who, outside of the team, knows about your dual identity?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :janus]}
     {:prompt/q "Who thinks the worst of your masked identity?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :janus]}
     {:prompt/q "Why do you care about the team?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :janus]}
     {:prompt/q "We saved the life of someone important, either to the city or to us. Who was it? Why are they important?"
      :prompt/type :prompt/team
      :prompt/playbook [:playbook/id :janus]}
     {:prompt/q "___ knew you from your civilian life first."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :janus]}
     {:prompt/q "You refused to tell ___ your secret identity when they asked."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :janus]}]
    :playbook/advances
    [{:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/minor
      :advance/description "Take drives from the Beacon's playbook"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/major
      :advance/description "Unlock your Moment of Truth after it's been used once"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/major
      :advance/description "Change playbooks"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook [:playbook/id :janus]}
     {:advance/type :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook [:playbook/id :janus]}]
    :playbook/moves
    [{:move/type :move/playbook-move
      :move/playbook [:playbook/id :janus]
      :move/name "The Mask"
      :move/description "You wear a mask and hide your real identity. Choose what Label you try to embody while wearing your mask: ❑ Freak ❑ Danger ❑ Savior ❑ Superior Once per session, you can affirm either your heroic or secret identity to switch your Mundane with your mask’s Label. When you reveal your secret identity to someone who didn’t know it already, mark potential."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :janus]
      :move/name "Game face"
      :move/description "When you commit yourself to save someone or defeat a terrible enemy, mark a condition and take +1 ongoing to all rolls in direct pursuit of that goal. At the end of any scene in which you don’t make progress towards that goal, mark a condition. When you fulfill your goal, mark potential."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :janus]
      :move/name "I am what you see"
      :move/description "When you spend time talking to someone about your identity, you can ask them which Label they want to impose on you; their player will tell you honestly. If you accept what they tell you, take +1 forward and either mark potential or clear a condition."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :janus]
      :move/name "Mild-mannered"
      :move/description "When you try to use your civilian identity to deceive, trick, or slip past someone, roll + Mundane. On a hit they buy your facade. On a 7-9, choose one: - you’re still under observation - you leave something incriminating behind - you’re forced to make a fool of yourself to sell it On a miss, one of your civilian obligations rears its ugly head."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :janus]
      :move/name "I'll save you!"
      :move/description "You’re willing to pay high costs to keep your loved ones safe. Reveal your secret identity to someone watching or mark a condition to defend a loved one as if you rolled a 12+."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :janus]
      :move/name "Dangerous web"
      :move/description "When you reveal a trap you’ve left for someone using your powers, roll + your mask’s Label. On a hit, your opponent trips into it, and you get an opening or opportunity against them. On a 10+, take +1 forward to pursuing it. On a miss, the trap inadvertently leads to a dangerous escalation."}]
    :playbook/extras
    [{:extra/playbook [:playbook/id :janus]
      :extra/id :secret-identity
      :extra/name "Secret Identity"
      :extra/description "Your mundane life comes with a series of obligations. Choose a total of three obligations. When time passes, roll + your Mundane to see how you’re managing your obligations. On a hit, things are going pretty well—you have an opportunity or advantage thanks to one of your obligations. On a 7-9, you’ve lapsed on one obligation, your choice. On a miss, you haven’t given your normal life anywhere near the attention it deserves; the GM chooses two obligations that are going to bite you in the butt."
      :extra/options
      [{:obligation/extra [:extra/name "Secret Identity"]
        :obligation/type "Jobs"
        :obligation/examples "Barista, intern, host/ess, salesperson, delivery person, fast-food worker, babysitter, dishwasher, tech support, waitress/er"}
       {:obligation/extra [:extra/name "Secret Identity"]
        :obligation/type "School"
        :obligation/examples "Schoolwork, athletic team, chess club, photography club, student government"}
       {:obligation/extra [:extra/name "Secret Identity"]
        :obligation/type "Home"
        :obligation/examples "Caring for someone, household chores, paying bills, surrogate parenting"}
       {:obligation/extra [:extra/name "Secret Identity"]
        :obligation/type "Social"
        :obligation/examples "Significant other, best friend, popularity, close relative, coach/teacher"}]}]
    }])

(def playbook-legacy
  [{:playbook/id :legacy}
   {:playbook/id :legacy
    :playbook/name "The Legacy"
    :playbook/influence "You’re a part of this team, for better or worse, and you care what they think. Give Influence to all of your teammates."
    :playbook/moment-of-truth "This is the moment when you prove how much the mantle belongs to you. You seize control of all your powers, and you defeat even impossible odds to prove you are worthy of the name you carry. You accomplish feats even your predecessors couldn’t do. Of course, after you prove something like that, you can expect still more responsibilities to be placed on your shoulders..."
    :playbook/ability "You have powers that match your general line. Choose one suite of powers, but pick two powers that you don’t have from your suite:"
    :playbook/abilities
    [{:ability/description "super strength, invincibility, eye beams, flight, super senses"
      :ability/playbook [:playbook/id :legacy]}
     {:ability/description "super speed, regeneration, phasing, speed-reading/learning, air manipulation"
      :ability/playbook [:playbook/id :legacy]}
     {:ability/description "athletic perfection, Holmesian deduction, gadgetry, intimidation, fearsome reputation"
      :ability/playbook [:playbook/id :legacy]}
     {:ability/description "divine armor, magic weaponry, mythic might, legendary speed, god-like beauty"
      :ability/playbook [:playbook/id :legacy]}
     {:ability/description "shadow control, shadow portals, mind-clouding, shadow cloak stealth, shadow senses"
      :ability/playbook [:playbook/id :legacy]}]
    :playbook/prompts
    [{:prompt/q "When did you officially become a part of your legacy?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :legacy]}
     {:prompt/q "What’s the greatest accomplishment of your legacy?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :legacy]}
     {:prompt/q "How does the public perceive your legacy?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :legacy]}
     {:prompt/q "How does your legacy tie into your reasons for being a hero?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :legacy]}
     {:prompt/q "Why do you care about the team?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :legacy]}
     {:prompt/q "All things considered, we did well and impressed an established hero. Who was it?"
      :prompt/type :prompt/team
      :prompt/playbook [:playbook/id :legacy]}
     {:prompt/q "You once got caught doing something that shames your legacy with ___."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :legacy]}
     {:prompt/q "You trust ___ and told them an important secret of your legacy."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :legacy]}]
    :playbook/advances
    [{:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/minor
      :advance/description "Take a Sanctuary from the Doomed playbook"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/minor
      :advance/description "Unlock the remaining two powers of your suite"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/major
      :advance/description "Unlock your Moment of Truth after it's been used once"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/major
      :advance/description "Change playbooks"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook [:playbook/id :legacy]}
     {:advance/type :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook [:playbook/id :legacy]}]
    :playbook/moves
    [{:move/type :move/playbook-move
      :move/playbook [:playbook/id :legacy]
      :move/name "Fight the good fight"
      :move/description "When you pull your punches while directly engaging a threat, you can roll + Savior instead of + Danger. If you do, you cannot choose to impress, surprise, or frighten your foe."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :legacy]
      :move/name "I know what I am"
      :move/description "Once per scene, when you defend a teammate you can shift Savior up and another Label down in addition to any other benefits from the move, even on a miss. If you do, add 1 Team to the pool."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :legacy]
      :move/name "Words of the past"
      :move/description "When you seek the guidance of one of your elders or a member of your legacy, tell them a problem you face, and ask them a question about the problem. They will answer it honestly, and tell you what to do. Take +1 ongoing if you listen. If you go your own way, mark potential."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :legacy]
      :move/name "The legacy matters"
      :move/description "When you take Influence over someone from your legacy (or give them Influence over you), mark potential and take +1 forward. When someone from your legacy causes your Labels to shift, mark potential and take +1 forward."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :legacy]
      :move/name "Never give up, never surrender"
      :move/description "When you take a powerful blow from someone with far greater power than you, use this move instead of the basic move. Roll + Savior. On a hit, you stand strong and choose one. On a 7-9, mark a condition. - you get an opportunity or opening against your attacker - you rally from the hit, and it inspires the team; add 1 Team to the pool - you keep your attacker’s attention On a miss, you go down hard but leave your opponent off balance and vulnerable."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :legacy]
      :move/name "Symbol of authority"
      :move/description "When you give an NPC an order based on authority they recognize, roll + Savior. On a hit, they choose one: - do what you say - get out of your way - attack you at a disadvantage - freeze On a 10+, you also take +1 forward against them. On a miss, they do as they please and you take -1 forward against them."}]
    :playbook/extras
    [{:extra/playbook [:playbook/id :legacy]
      :extra/id :legacy
      :extra/name "Legacy"
      :extra/description "Your legacy is an important part of Halcyon City. Name the different members of your legacy (at least two): ___ is still active and prominent in the city. ___ is retired and quite judgmental. ___ is the next possible member of your legacy. ___ is the greatest opponent your legacy ever faced... and is still at large. Whenever time passes, roll + Savior to see how the members of your legacy feel or react to your most recent exploits. Before rolling, ask the other players to answer these questions about your performance. Take -1 to the roll for each “no” answer: have you been upholding the traditions of your legacy? have you maintained the image of your legacy? have you made the other members of your legacy proud?"}]}])

(def playbook-nova
  [{:playbook/id :nova}
   {:playbook/id :nova
    :playbook/name "The Nova"
    :playbook/influence "Choose your demeanor: happy facade or locked down. If you choose happy facade, give Influence to three teammates. If you choose locked down, give Influence to one teammate."
    :playbook/moment-of-truth "Your mind’s eye opens, and you can see the world around you like never before. You can control it, at will, with ease. Of course, warping reality tends to have ramifications down the line, but in your moment of godhood... how could you possibly be worried?"
    :playbook/ability "You can fundamentally control the world around you. Choose one option from the list as the broad base of your control."
    :playbook/abilities
    [{:ability/description "telekinesis and telepathy"
      :ability/playbook [:playbook/id :nova]}
     {:ability/description "biokenesis"
      :ability/playbook [:playbook/id :nova]}
     {:ability/description "gravity manipulation"
      :ability/playbook [:playbook/id :nova]}
     {:ability/description "sorcery"
      :ability/playbook [:playbook/id :nova]}
     {:ability/description "elemental control"
      :ability/playbook [:playbook/id :nova]}
     {:ability/description "cosmic energies"
      :ability/playbook [:playbook/id :nova]}]
    :playbook/prompts
    [{:prompt/q "When did you first use your powers?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :nova]}
     {:prompt/q "Who was the first person you accidentally hurt with your powers?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :nova]}
     {:prompt/q "Who, outside the team, helps you control your powers?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :nova]}
     {:prompt/q "Why do you continue to use your powers?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :nova]}
     {:prompt/q "Why do you care about the team?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :nova]}
     {:prompt/q "We destroyed our surroundings in the fight. Where was it? What did we destroy?"
      :prompt/type :prompt/team
      :prompt/playbook [:playbook/id :nova]}
     {:prompt/q "You hang out all the time with ___ to blow off steam."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :nova]}
     {:prompt/q "You once hurt ___ when you lost control of your powers."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :nova]}]
    :playbook/advances
    [{:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/minor
      :advance/description "Unlock three new flares"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/minor
      :advance/description "Unlock three new flares"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/minor
      :advance/description "Take The Bull's Heart from the Bull playbook"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/major
      :advance/description "Unlock your Moment of Truth after you've used it once"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/major
      :advance/description "Change playbooks"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook [:playbook/id :nova]}
     {:advance/type :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook [:playbook/id :nova]}]
    :playbook/extras
    [{:extra/playbook [:playbook/id :nova]
      :extra/name "Burn"
      :extra/description "When you charge up your powers, roll + conditions you currently have marked. On a hit, hold 3 burn. On a 7-9, mark a condition. On a miss, hold 2 burn and mark three conditions. Spend your burn on your flares. You lose all burn at the end of the scene. Choose four flares."
      :extra/options
      [{:burn/extra [:extra/name "Burn"]
        :burn/name "Reality storm"
        :burn/description "You channel a destructive burst with your powers. Spend 1 burn to directly engage a threat using your powers, rolling + Freak instead of + Danger. If you do, you will cause unwanted collateral damage unless you spend another burn."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Shielding"
        :burn/description "You call up a fast protective field to stop a danger. Spend 1 burn to defend someone else from an immediate threat, rolling + Freak instead of + Savior."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Constructs"
        :burn/description "Spend 1 burn to create any object with your powers, up to the size of a person. Spend an additional burn to animate it independently of yourself. The construct dissolves at the end of the scene."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Moat"
        :burn/description "Spend 1 burn to create a barrier that will hold back threats as long as you keep your attention on it. The GM may call for you to spend another burn if the barrier is threatened by particularly powerful enemies."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Worship"
        :burn/description "You put out a tremendous display of your might. Spend 1 burn to awe an audience into silence, respect, and attention when you unleash your powers."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Worship"
        :burn/description "You put out a tremendous display of your might. Spend 1 burn to awe an audience into silence, respect, and attention when you unleash your powers."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Move"
        :burn/description "Spend 1 burn to move to any place you choose within the scene, breaking through or slipping past any barriers or restraints in your way. Spend a second burn to move to any place you’ve previously been."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Boost"
        :burn/description "Spend 1 burn to supercharge a teammate’s efforts with your powers, giving them a +1 bonus to their roll as if you had spent Team from the pool."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Overcharge"
        :burn/description "You channel the full capacity of your incredible powers to overcome an obstacle, reshape your environment, or extend your senses. Spend 2 burn to take a 10+ when you unleash your powers."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Elemental awareness"
        :burn/description "Spend 1 burn and mark a condition to open your mind up to the world around you with your powers. You can ask any one question about the world around you, and the GM will answer honestly."}
       {:burn/extra [:extra/name "Burn"]
        :burn/name "Snatch"
        :burn/description "Spend 1 burn to use your powers to seize any one object up to the size of a person from someone within view."}
       ]}]
    }])

(def playbook-outsider
  [{:playbook/id :outsider}
   {:playbook/id :outsider
    :playbook/name "The Outsider"
    :playbook/influence "Choose your demeanor: haughty or cheerful. If you’re haughty, you think you’re better than them. Give no one Influence. If you’re cheerful, you’re thrilled to be here. Give everyone Influence over you."
    :playbook/moment-of-truth "You embrace your home and call them for aid. They will answer your call—in force!—arriving exactly when you need them to turn the tide. They fight and serve you for the rest of the battle. Of course, when all is said and done...they’d probably like to take you home with them. You did, after all, just prove yourself worthy."
    :playbook/ability "You can fly, and you’re pretty tough. Choose any two of the following:"
    :playbook/abilities
    [{:ability/description "density control"
      :ability/playbook [:playbook/id :outsider]}
     {:ability/description "heliokenesis"
      :ability/playbook [:playbook/id :outsider]}
     {:ability/description "stunning beauty or pheromones"
      :ability/playbook [:playbook/id :outsider]}
     {:ability/description "radical shapeshifting"
      :ability/playbook [:playbook/id :outsider]}
     {:ability/description "alien weaponry"
      :ability/playbook [:playbook/id :outsider]}
     {:ability/description "telepathy and mind blasts"
      :ability/playbook [:playbook/id :outsider]}]
    :playbook/prompts
    [{:prompt/q "Where do you come from?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :outsider]}
     {:prompt/q "Why did you come to Earth?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :outsider]}
     {:prompt/q "Why do you want to stay here (for now at least)?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :outsider]}
     {:prompt/q "Why do your people want you to come home?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :outsider]}
     {:prompt/q "Why do you care about the team?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :outsider]}
     {:prompt/q "We didn’t trust each other at first, but that changed. How? Why?"
      :prompt/type :prompt/team
      :prompt/playbook [:playbook/id :outsider]}
     {:prompt/q "You've been learning about Earth by spending time with ___."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :outsider]}
     {:prompt/q "You have a crush on ___ but you keep it under wraps."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :outsider]}]
    :playbook/moves
    [{:move/type :move/playbook-move
      :move/playbook [:playbook/id :outsider]
      :move/name "Belong in two worlds"
      :move/description "You have the resources that come with your station. Whenever you contact your people, roll + Superior. On a 10+, hold 3. On a 7-9, hold 2. On a miss, hold 1, but your people make an uncomfortable demand of you. Spend your hold 1 for 1 to: - receive a useful piece of alien technology that will allow you to use any ability from another playbook once (choose the ability when you spend the hold) - consult your people’s knowledge to ask the GM a question about the current situation - clear a condition through the comfort of contact with your home"}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :outsider]
      :move/name "Alien tech"
      :move/description "When you alter a human device with your alien technology, roll + Freak. On a hit, you create a device that can do something impossible once and then fizzle. When you roll a 10+, choose one: - it works exceptionally well - you get an additional use out of it On a miss, the device works, but it has a completely unintended side effect that the GM will reveal when you use it."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :outsider]
      :move/name "Alien ways"
      :move/description "Whenever you openly disregard or undermine an important Earth custom in favor of one of your own people’s customs, shift Superior up and any other Label down."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :outsider]
      :move/name "Kirby-craft"
      :move/description "You have a vehicle, something from your home. Detail its look, and choose two strengths and two weaknesses. When you are flying your ship, you can use it to unleash your powers, directly engage a threat, or defend someone using Superior. Strengths: Fast & maneuverable, chameleon plating, powerful weaponry, regenerating, dimension-shifting, size-shifting, telepathic Weaknesses: Bizarre fuel source, susceptible to ___, easily detectable, slow and clumsy, unarmed, difficult to repair"}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :outsider]
      :move/name "The best of them"
      :move/description "When you comfort or support someone by telling them how they exemplify the best parts of Earth, roll + Freak instead of + Mundane."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :outsider]
      :move/name "Not so different after all"
      :move/description "When you talk about your home, roll + Freak. On a 10+, choose two. On a 7-9, choose one. During the conversation, you: - confess a flaw of your home; add 1 Team to the pool - mislead them about your home; take Influence over them - describe the glories of your home; clear a condition On a miss, you inadvertently reveal more about yourself than you planned; tell them a secret or vulnerability you haven’t shared with Earthlings before now."}]
    :playbook/advances
    [{:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/minor
      :advance/description "Choose two new abilities from any playbook as you come into your own"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/minor
      :advance/description "You adopt a human life; take Secret Identity and The Mask from the Janus playbook"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/major
      :advance/description "Unlock your Moment of Truth after you've used it once"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/major
      :advance/description "Change playbooks"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook [:playbook/id :outsider]}
     {:advance/type :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook [:playbook/id :outsider]}]
    }])

(def playbook-protege
  [{:playbook/id :protege}
   {:playbook/id :protege
    :playbook/name "The Protégé"
    :playbook/influence "Choose your demeanor: playful or business. If you choose playful, give Influence to two teammates. If you choose business, give Influence to no teammates."
    :playbook/moment-of-truth "The moment that you show who you really are: your mentor, or something different. You can do whatever your mentor could do and more. You can do the incredible, even the things they always failed to accomplish. Of course, they’re not going to see you the same way, no matter which path you choose…"
    :playbook/ability "You are someone’s protégé. Your powers largely mimic theirs, but each of you is in some way unique. Pick one ability you both share and one ability for each of you that is uniquely yours."
    :playbook/abilities
    [{:ability/description "superhuman physique"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "weapons and gadgets"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "stealth"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "detective skills"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "hacking"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "power mimicry"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "body elasticity"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "powerful armor"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "telepathy/telekinesis"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "intimidation and fear"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "impossible fighting skills"
      :ability/playbook [:playbook/id :protege]}
     {:ability/description "elemental control"
      :ability/playbook [:playbook/id :protege]}]
    :playbook/moves
    [{:move/type :move/playbook-move
      :move/playbook [:playbook/id :protege]
      :move/name "Been reading the files"
      :move/description "You’ve learned about the superhuman world through your mentor’s resources. When you first encounter an important superpowered phenomenon (your call), roll + Superior. On a hit, tell the team one important detail you’ve learned from your studies. The GM will tell you what, if anything, seems different from what you remember. On a 10+, ask the GM a follow-up question; they will answer it honestly. On a miss, the situation is well outside your base of knowledge; the GM will tell you why."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :protege]
      :move/name "Captain"
      :move/description "When you enter battle as a team, add an extra Team to the pool and carry +1 forward if you are the leader."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :protege]
      :move/name "Venting frustration"
      :move/description "When you directly engage while you are Angry, you can roll + the Label your mentor denies and clear Angry."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :protege]
      :move/name "Fireside chat"
      :move/description "When you seek advice from your mentor, roll + the Label they embody. On a hit they will tell you what to do. On a 10+, mark potential if you follow their advice, and take +1 ongoing to follow through. On a 7-9, you get +1 forward to see it through if you do it their way. On a miss, they don’t have time for you because something big has gone down; mark a condition, GM’s choice."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :protege]
      :move/name "Be mindful of your surroundings"
      :move/description "When you assess the situation before entering into a fight, you may ask one additional question, even on a miss."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :protege]
      :move/name "Heroic tradition"
      :move/description "When you give someone the advice that you think your mentor would give, you can roll + the Label your mentor embodies to comfort or support someone, instead of rolling + Mundane."}]
    :playbook/prompts
    [{:prompt/q "How did you first meet your mentor?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :protege]}
     {:prompt/q "When and why did you choose to train with them?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :protege]}
     {:prompt/q "Why did they agree to train you?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :protege]}
     {:prompt/q "Who else, outside of the team, knows about your training?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :protege]}
     {:prompt/q "Why do you care about the team?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :protege]}
     {:prompt/q "We stuck together after all was said and done. Why? How’d we keep in contact?"
      :prompt/type :prompt/team
      :prompt/playbook [:playbook/id :protege]}
     {:prompt/q "You and ___ teamed up a few times before the rest of you came together."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :protege]}
     {:prompt/q "Your mentor is cautious; they asked you to keep an eye on ___."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :protege]}]
    :playbook/advances
    [{:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/minor
      :advance/description "Add +2 to the Label your mentor embodies or denies"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/minor
      :advance/description "Choose up to four more resources from your mentor"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/major
      :advance/description "Unlock your Moment of Truth after you've used it once"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/major
      :advance/description "Change playbooks"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook [:playbook/id :protege]}
     {:advance/type :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook [:playbook/id :protege]}]
    :playbook/extras
    [{:extra/playbook [:playbook/id :protege]
      :extra/name "Mentor"
      :extra/description "You have a mentor, someone who’s taught you, trained you, given you aid, or raised you up. Someone who might have confined you a bit too rigidly to a single path. Which Label do they embody, and which do they deny?"}
     {:extra/playbook [:playbook/id :protege]
      :extra/name "Mentor's Resources"
      :extra/description "Choose up to three resources that your mentor gave you and the team:"
      :extra/options
      (mapv (fn [resource] {:mentor-resource/extra [:extra/name "Mentor's Resources"]
                            :mentor-resource/description resource})
            ["a hidden base" "a vehicle" "a supercomputer" "communicators" "surveillance equipment"
             "false identites" "badges of authority" "a chem lab" "a med lab" "a teleportal"
             "a weapon of last resort" "security systems" "simple robots"])}]}])

(def playbook-transformed
  [{:playbook/id :transformed}
   {:playbook/id :transformed
    :playbook/name "The Transformed"
    :playbook/influence "You try not to care what other people think, even if you can’t
shut everyone out. Give Influence to one teammate."
    :playbook/moment-of-truth "It’s so easy to forget that you’re not your body, and you’re not the voice in your head—you’re both. Be the monster, and save them anyway. Smash down walls, and speak softly. Because when you embrace it, you can do anything. Of course, putting on a display like this is sure to rile up those who see only the monster when they look at you…"
    :playbook/ability "You appear obviously and clearly monstrous, and your powers are tied to your appearance. Choose two, and describe how they make you grotesque."
    :playbook/abilities
    [{:ability/description "impenetrable armor"
      :ability/playbook [:playbook/id :transformed]}
     {:ability/description "inhuman might"
      :ability/playbook [:playbook/id :transformed]}
     {:ability/description "plant affinity"
      :ability/playbook [:playbook/id :transformed]}
     {:ability/description "superhuman senses"
      :ability/playbook [:playbook/id :transformed]}
     {:ability/description "technopathy"
      :ability/playbook [:playbook/id :transformed]}
     {:ability/description "transmuting flesh"
      :ability/playbook [:playbook/id :transformed]}]
    :playbook/prompts
    [{:prompt/q "Who were you before?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :transformed]}
     {:prompt/q "When did you change?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :transformed]}
     {:prompt/q "What caused it?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :transformed]}
     {:prompt/q "Who, outside of the team, is helping you understand your new body?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :transformed]}
     {:prompt/q "Why don’t you just try to hide yourself away?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :transformed]}
     {:prompt/q "Why do you care about the team?"
      :prompt/type :prompt/individual
      :prompt/playbook [:playbook/id :transformed]}
     {:prompt/q "We drew attention and ire from plenty during the fight. One important person in particular now hates and fears us. Who is it?"
      :prompt/type :prompt/team
      :prompt/playbook [:playbook/id :transformed]}
     {:prompt/q "___ comforted you when you were at your lowest."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :transformed]}
     {:prompt/q "___ knew you before you changed."
      :prompt/type :prompt/relationship
      :prompt/playbook [:playbook/id :transformed]}]
    :playbook/moves
    [{:move/type :move/playbook-move
      :move/playbook [:playbook/id :transformed]
      :move/name "I am not my body"
      :move/description "When you take a powerful physical blow, you may roll as if you had two fewer conditions marked. If you do, on a 10+ you must choose to lose control of yourself in a terrible way."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :transformed]
      :move/name "Not human enough"
      :move/description "When you directly engage a threat in a terrifying fashion, mark a condition to choose an additional option, even on a miss."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :transformed]
      :move/name "Unstoppable"
      :move/description "When you smash your way through scenery to get to or away from something, roll + Danger. On a hit, the world breaks before you, and you get what you want. On a 7-9, choose one: mark a condition, leave something behind, or take something with you. On a miss, you smash through, but leave devastation in your wake or wind up somewhere worse, GM’s choice."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :transformed]
      :move/name "Coming for you"
      :move/description "When you mark a condition, take +1 forward against the person you most blame for causing it."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :transformed]
      :move/name "Wish I could be"
      :move/description "When you comfort or support someone, if you tell them what you most envy about them, you can roll + Freak instead of + Mundane."}
     {:move/type :move/playbook-move
      :move/playbook [:playbook/id :transformed]
      :move/name "Be the monster"
      :move/description "When you frighten, intimidate, or cow others with your monstrous form, roll + Freak. On a hit, they are thrown off and make themselves vulnerable to you, or they flee. On a 10+, choose one. On a 7-9, choose two. - you frighten others you had not intended to scare - you hurt someone or break something you shouldn’t have - you feel like more of a monster afterward; mark a condition (GM’s choice) On a miss, they react with violence, hatred, and paranoia, and you suffer the brunt of it."}]
    :playbook/advances
    [{:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/minor
      :advance/description "Take another move from your playbook"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/minor
      :advance/description "Take a move from another playbook"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/minor
      :advance/description "Someone permanently loses Influence over you; add +1 to a Label"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/minor
      :advance/description "Rearrange your labels as you choose, and add +1 to a Label"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/minor
      :advance/description "Unlock your Moment of Truth"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/minor
      :advance/description "Take a doom, doomtrack, and doomsigns from the Doomed playbook"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/minor
      :advance/description "Mutate further and reveal another two new abilities (chosen from any playbook)"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/major
      :advance/description "Unlock your Moment of Truth after you've used it once"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/major
      :advance/description "Change playbooks"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/major
      :advance/description "Take an adult move"
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/major
      :advance/description "Lock a Label, and add +1 to a Label of your choice."
      :advance/playbook [:playbook/id :transformed]}
     {:advance/type :advance/major
      :advance/description "Retire from the life or become a paragon of the city"
      :advance/playbook [:playbook/id :transformed]}]
    }])

(def playbooks
  (concat playbook-beacon
          playbook-bull
          playbook-delinquent
          playbook-doomed
          playbook-janus
          playbook-legacy
          playbook-nova
          playbook-outsider
          playbook-protege
          playbook-transformed
          ))

(def abilities-schema
  [{:db/ident :ability/description
    :db/unique :db.unique/identity
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}

   {:db/ident :ability/playbook
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}])

(def prompt-schema
  [
   ;; :prompt/q is also the id - no duplicates
   {:db/ident :prompt/q
    :db/valueType :db.type/string
    :db/unique :db.unique/identity
    :db/cardinality :db.cardinality/one}

   ;; an enum for :prompt/type
   {:db/ident :prompt/individual}
   {:db/ident :prompt/team}
   {:db/ident :prompt/relationship}
   {:db/ident :prompt/type
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}

   {:db/ident :prompt/playbook
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/many}])

(def move-schema
  [{:db/ident :move/playbook-move}
   {:db/ident :move/team-move}
   {:db/ident :move/type
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}

   {:db/ident :move/name
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/unique :db.unique/identity}

   {:db/ident :move/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}

   {:db/ident :move/playbook
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/many}])

(def team-moves
  [{:move/type :move/team-move
    :move/name "Share a triumphant celebration"
    :move/description "When you share a triumphant celebration with someone, tell them how they’re awesome and add a Team to the pool. If they tell you how you’re awesome in return, add another Team to the pool."}
   {:move/type :move/team-move
    :move/name "Share a vulnerability"
    :move/description "When you share a vulnerability or weakness with someone, ask them to confirm or deny that you should be here. If they confirm it, mark potential and give them Influence over you. If they deny it, mark Angry and shift one Label up and one Label down, your choice."}])

;; the same advance can be present multiple times for the same playbook
(def advance-schema
  [{:db/ident :advance/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}

   {:db/ident :advance/type
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :advance/minor}
   {:db/ident :advance/major}

   {:db/ident :advance/playbook
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/many}])

(def extras-schema
  [{:db/ident :extra/name
    :db/valueType :db.type/string
    :db/unique :db.unique/identity
    :db/cardinality :db.cardinality/one}

   {:db/ident :extra/id
    :db/valueType :db.type/keyword
    :db/unique :db.unique/identity
    :db/cardinality :db.cardinality/one}

   {:db/ident :extra/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}
   {:db/ident :extra/playbook
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :extra/options
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/many}
   ])

(def drive-schema
  [{:db/ident :drive/extra
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :drive/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}])

(def heart-role-schema
  [{:db/ident :heart-role/extra
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :heart-role/name
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}
   {:db/ident :heart-role/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}])

(def doomsign-schema
  [{:db/ident :doomsign/extra
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :doomsign/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}])

(def doombringer-schema
  [{:db/ident :doombringer/extra
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :doombringer/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}])

(def sanctuary-prereq-schema
  [{:db/ident :sanctuary-prereq/extra
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :sanctuary-prereq/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}])

(def obligation-schema
  [{:db/ident :obligation/extra
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :obligation/type
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}
   {:db/ident :obligation/examples
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}])

(def burn-schema
  [{:db/ident :burn/extra
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :burn/name
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}
   {:db/ident :burn/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}])

(def mentor-resource-schema
  [{:db/ident :mentor-resource/extra
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :mentor-resource/description
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}])

(defn load-schemas []
  (d/transact db/conn (vec (concat playbook-schema
                                   abilities-schema
                                   prompt-schema
                                   move-schema
                                   advance-schema
                                   extras-schema
                                   drive-schema
                                   heart-role-schema
                                   doomsign-schema
                                   doombringer-schema
                                   sanctuary-prereq-schema
                                   obligation-schema
                                   burn-schema
                                   mentor-resource-schema))))

(defn load-data []
  (d/transact db/conn (vec (concat playbooks
                                   team-moves))))

(comment
  (db/reset-db)

  (load-schemas)
  (load-data)

  (d/pull @db/conn ["*" {:playbook/moves ["*"] :playbook/prompts ["*"] :playbook/abilities ["*"] :playbook/advances ["*"] }] [:playbook/id :transformed])


  (->> (d/q {:query '{:find [?advances]
                      :where [[?e :playbook/id :beacon]
                              [?e :playbook/advances ?advances]]}
             :args [@db/conn]})
       (map #(into {} (d/entity @db/conn (first %)))))

  (#:db{:id 100} #:db{:id 106} #:db{:id 105} #:db{:id 99} #:db{:id 95} #:db{:id 96} #:db{:id 103} #:db{:id 102} #:db{:id 97} #:db{:id 107} #:db{:id 108} #:db{:id 101} #:db{:id 104} #:db{:id 98})

  nil
  #{[100] [106] [105] [99] [95] [96] [103] [102] [97] [107] [108] [101] [104] [98]}

  (d/pull @db/conn [{:playbook/advances [:advance/description {:advance/type [:db/ident]}]}] [:playbook/id :beacon])





  )
