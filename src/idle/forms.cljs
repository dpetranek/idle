(ns idle.forms
  (:require [re-frame.core :as rf]
            [idle.common :as common]))

;; Accepts a unique form-id to store the form state, and a map of form field name to
;; field initial value. Also accepts a validator function for the whole form that takes
;; a map of fields and values and returns a map fields and error keys, nil if good.
(rf/reg-event-db
 :form/register-form
 [rf/debug]
 (fn [db [_ {:keys [form-id cmd fields handlers]}]]
   (assoc db form-id (merge {:values fields :initial fields :cmd cmd :handlers handlers}))))

(rf/reg-event-db
 :form/unregister-form
 [rf/debug]
 (fn [db [_ form-id]]
   (dissoc db form-id)))

(rf/reg-sub
 :form/field-value
 (fn [db [_ form-id field-key]]
   (get-in db [form-id :values field-key])))

(rf/reg-sub
 :form/field-error
 (fn [db [_ form-id field-key]]
   (get-in db [form-id :errors field-key])))

(rf/reg-sub
 :form/form-error
 (fn [db [_ form-id]]
   (get-in db [form-id :error])))

(rf/reg-event-db
 :form/update-field
 [rf/debug]
 (fn [db [_ form-id field-key value]]
   (assoc-in db [form-id :values field-key] value)))

(rf/reg-event-fx
 :form/start-submit
 [rf/debug]
 (fn [{:keys [db]} [_ form-id]]
   (let [{:keys [validator payload]} (get-in db [form-id :handlers])]
     (cond validator {:db db :dispatch [validator form-id]}
           payload   {:db db :dispatch [payload form-id]}
           :else     {:db db :dispatch [:form/submit form-id]}))))

(rf/reg-event-fx
 :form/submit
 [rf/debug]
 (fn [{:keys [db]} [_ form-id]]
   (let [{:keys [cmd values payload handlers]} (get db form-id)
         {:keys [success failure]} handlers]
     {:db (assoc-in db [form-id :submitting?] true)
      :dispatch [::common/request {:method :post
                                   :uri "/api/command"
                                   :params {:cmd cmd :payload (or payload values)}
                                   :on-success [:form/submit-success form-id success]
                                   :on-failure [:form/submit-failure form-id failure]}]})))

(rf/reg-event-fx
 :form/submit-success
 [rf/debug]
 (fn [{:keys [db]} [_ form-id success-action response]]
   (let [form       (get db form-id)
         reset-form (-> form
                        (assoc :submitting? false)
                        (assoc :values (:initial form))
                        (dissoc :errors))]
     (-> {}
         (assoc :db (assoc db form-id reset-form))
         (cond-> success-action
           (assoc :dispatch [success-action response]))))))

(rf/reg-event-fx
 :form/submit-failure
 [rf/debug]
 (fn [{:keys [db]} [_ form-id failure-action error]]
   (let [errors (:errors (:response error))]
     (-> {}
         (assoc :db (-> db
                        (assoc-in [form-id :submitting?] false)
                        (cond-> errors (assoc-in [form-id :errors] errors))
                        (cond-> (not errors) (assoc-in [form-id :error] error))))
         (cond-> failure-action
           (assoc :dispatch [failure-action error]))))))

(defn field [{:as attrs :keys [form-id name]}]
  (let [value (rf/subscribe [:form/field-value form-id name])
        error (rf/subscribe [:form/field-error form-id name])]
    (fn []
      [:div.field
       [:input (merge
                {:value @value
                 :on-change #(rf/dispatch [:form/update-field form-id name
                                           (-> % .-target .-value)])}
                (dissoc attrs :form-id))]
       [:small.error @error]])))
