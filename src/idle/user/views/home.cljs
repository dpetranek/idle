(ns idle.user.views.home
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [idle.common :as common]))

(kf/reg-controller
 :route.user/home
 {:params (fn [route] (when (= (-> route :data :name) :route.user/home)
                        (:user-id (:path-params route))))
  :start (fn [_ user-id] [::common/resolve-data :route.user/home
                          [:user/id (uuid user-id)]
                          [:user/id :user/username {:user/game ["*"]} {:user/character ["*"]}]])
  :stop [::common/clean-resolve :route.user/home]})

(defn user-page []
  (let [user (rf/subscribe [::common/resolve-data :route.user/home])]
    (fn []
      [:div [:h1 "user"]
       [:ul
        (for [game (:user/game @user)] ^{:key (:heist/id game)}
          [:li [:a {:href (kf/path-for [:route.heist/play {:game-id (:heist/id game)}])} "Heist"] ])]])))
