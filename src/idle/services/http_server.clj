(ns idle.services.http-server
  (:require [immutant.web :as http]
            [idle.api :as api]))

(defn start! []
  (println "Starting sietch http-server on port 3333" )
  (http/run api/app {:host "localhost" :port 3333}))

(defn stop! [server]
  (http/stop server))
