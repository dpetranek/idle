(ns idle.services.datahike
  (:require [datahike.api :as d]
            [taoensso.timbre :as timbre]))

#_(def db-uri "datahike:file:///tmp/idle.datahike")
(def db-uri {:backend :file :path "local-db"})
(def mem-uri "datahike:mem://mem-example")
(def boggle-db-uri {:backend :file :path "resources/dbs/boggle"})
(def idle-uri {:backend :file :path "resources/dbs/boggle"})
(def idle-dev-uri "datahike:mem://idle-dev")


(comment
  #_(d/create-database db-uri)
  (d/delete-database mem-uri)
  )

(defn reset-db []
  (d/delete-database idle-dev-uri)
  (d/create-database mem-uri)
  )

(d/create-database mem-uri)
(when (not (d/database-exists? boggle-db-uri))
  (d/create-database boggle-db-uri))

(def conn (d/connect mem-uri))

(def boggle-db (d/connect boggle-db-uri))

(def idle-schema
  [{:db/ident :user/id
    :db/valueType :db.type/uuid
    :db/cardinality :db.cardinality/one
    :db/unique :db.unique/identity}
   {:db/ident :user/username
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/unique :db.unique/identity}
   {:db/ident :user/pass-hash
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}
   {:db/ident :user/email
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}
   {:db/ident :user/session-token
    :db/valueType :db.type/uuid
    :db/cardinality :db.cardinality/one
    :db/unique :db.unique/identity}
   {:db/ident :user/game
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/many}
   {:db/ident :user/character
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/many}

   ])

(def honey-heist-schema
  [{:db/ident :heist/id
    :db/valueType :db.type/uuid
    :db/cardinality :db.cardinality/one
    :db/unique :db.unique/identity}
   {:db/ident :heist/organizer
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one}
   {:db/ident :heist/location
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one}
   {:db/ident :heist/location-descr
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one}
   {:db/ident :heist/security
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/many}
   {:db/ident :heist/prize
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one}
   {:db/ident :heist/twist
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one}
   {:db/ident :heist/manager
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :heist/bear
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/many}

   {:db/ident :bear/id
    :db/valueType :db.type/uuid
    :db/cardinality :db.cardinality/one
    :db/unique :db.unique/identity}
   {:db/ident :bear/player
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :bear/descriptor
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one}
   {:db/ident :bear/type
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one}
   {:db/ident :bear/skill
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one}
   {:db/ident :bear/role
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/one}
   {:db/ident :bear/hats
    :db/valueType :db.type/keyword
    :db/cardinality :db.cardinality/many}
   {:db/ident :bear/name
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one}
   {:db/ident :bear/heist
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one}
   {:db/ident :bear/bear
    :db/valueType :db.type/number
    :db/cardinality :db.cardinality/one}
   {:db/ident :bear/criminal
    :db/valueType :db.type/number
    :db/cardinality :db.cardinality/one}])

(defn initialize-db [& x]
  (timbre/info "initializing db" (pr-str x))
  (when (not (d/database-exists? {:backend :file :path "resources/dbs/idle"}))
    (d/create-database {:backend :file :path "resources/dbs/idle"})))

(defn connect-db []
  (d/connect {:backend :file :path "resources/dbs/idle"}))

(defn migrate-db [conn]
  (d/transact conn idle-schema)
  (d/transact conn honey-heist-schema))

(defn reset-db []
  (d/delete-database {:backend :file :path "resources/dbs/idle"})
  (initialize-db))

#_(when (not (d/database-exists? idle-dev-uri))
    (d/create-database idle-dev-uri)
    (def idle-dev (d/connect idle-dev-uri))
    (d/transact idle-dev idle-schema)
    (d/transact idle-dev honey-heist-schema))

(def idle-dev (d/connect {:backend :file :path "resources/dbs/idle"}))
(comment
  (d/transact idle-dev idle-schema)
  (d/transact idle-dev honey-heist-schema)

  (d/pull @idle-dev ["*"] [:user/username "dan"])
  {:db/id 26, :user/id #uuid "ae83bdf4-8b8a-42bf-afbd-218e2ff93f63", :user/pass-hash "bcrypt+sha512$b877e2ebfcc0d88f12efdb57c17fdac3$12$ea41e3ddde001badf0368c05eb244b9fd52f5860adc50c5f", :user/session-token #uuid "d86f2784-e772-4e96-adf2-4cb638505b85", :user/username "dan"}

  (d/pull @idle-dev [{:heist/bear [:bear/id {:bear/player [:user/username]}]}] [:heist/id #uuid "1ac7f336-1ef4-48f3-98a8-e8173e153faf"])
  #:heist{:bear [#:bear{:id #uuid "ae62862c-b979-4d2d-99f9-a1a9d6ad6558", :player #:user{:username "murray"}} #:bear{:id #uuid "27afe2c3-1705-4f51-b9d7-b593a7c809d1", :player #:user{:username "arthur"}} #:bear{:id #uuid "80553e9d-b3e0-43a3-82d4-6c680ee93158", :player #:user{:username "dan"}} #:bear{:id #uuid "7ec453fe-452e-414c-bb8e-dfaccd76a43b", :player #:user{:username "dan"}} #:bear{:id #uuid "e68837d6-a5af-4963-bca7-791821275714", :player #:user{:username "dan"}}]}
  (d/pull @idle-dev [:bear/bear :bear/criminal {:bear/heist [{:heist/manager [:user/id]}
                                                             {:heist/bear [{:bear/player [:user/id]}]}]}]
          [:bear/id #uuid "27afe2c3-1705-4f51-b9d7-b593a7c809d1"])
  #:bear{:bear 3, :criminal 3, :heist #:heist{:manager #:user{:id #uuid "f2637742-e416-404b-92a1-ada6ce7396b1"}, :bear [#:bear{:player #:user{:id #uuid "a31fc5ec-0e74-4ee3-ad86-bbe0847b2d5a"}} #:bear{:player #:user{:id #uuid "fbc8db94-0442-41a0-ad71-289ad95f18a2"}} #:bear{:player #:user{:id #uuid "f2637742-e416-404b-92a1-ada6ce7396b1"}} #:bear{:player #:user{:id #uuid "f2637742-e416-404b-92a1-ada6ce7396b1"}} #:bear{:player #:user{:id #uuid "f2637742-e416-404b-92a1-ada6ce7396b1"}}]}}

  (let [{bear-stat :bear/bear
         criminal-stat :bear/criminal
         {{manager-id :user/id} :heist/manager
          players :heist/bear
          heist-id :heist/id :as heist} :bear/heist}

        (d/pull @idle-dev [:bear/bear :bear/criminal {:bear/heist [:heist/id
                                                                   {:heist/manager [:user/id]}
                                                                   {:heist/bear [{:bear/player [:user/id]}]}]}]
                [:bear/id #uuid "27afe2c3-1705-4f51-b9d7-b593a7c809d1"])]
    [heist heist-id players manager-id])

  {:heist/twist :rigged-to-blow, :heist/manager #:db{:id 26}, :heist/location :wilderness-retreat, :heist/id #uuid "ed864ea4-bc20-49ba-94bc-3312b6881132", :heist/bear [{:bear/type :grizzly, :bear/id #uuid "1d5c2d9b-e8a6-48bd-92ab-a8d674a6ddd0", :bear/criminal 3, :bear/role :thief, :bear/player #:db{:id 26}, :bear/bear 3, :db/id 29, :bear/skill :terrify, :bear/hats [:crown], :bear/heist #:db{:id 28}, :bear/descriptor :incompetent}], :heist/organizer :spoilt-trust-fund-kid, :heist/location-descr :creepy, :db/id 28, :heist/prize :haunted-lincoln-beehive, :heist/security [:cctv-network :impenetrable-vault]}

  {:heist/twist :rigged-to-blow, :heist/manager #:db{:id 26}, :heist/location :wilderness-retreat, :heist/id #uuid "ed864ea4-bc20-49ba-94bc-3312b6881132", :heist/bear [#:db{:id 29}], :heist/organizer :spoilt-trust-fund-kid, :heist/location-descr :creepy, :db/id 28, :heist/prize :haunted-lincoln-beehive, :heist/security [:cctv-network :impenetrable-vault]}



  (:user/username (into {} (d/entity @idle-dev [:user/username "bob"])))

  (into {} (d/entity @idle-dev [:user/session-token #uuid "bfc6fdd6-5179-4588-95db-73c12b854bfb"]))

  (d/pull @idle-dev ["*"] [:heist/id #uuid "4c6947ec-79e7-4327-ad51-befdb6e43581"])

  (d/q {:query '{:find [?u]
                 :where [[?e :user/username ?u]] }
        :args [@idle-dev]})
  #{["bob"]}


  )
