(ns idle.othello.data
  (:require [clojure.spec.alpha :as s]))

(s/def ::player #{:dark :light})
(s/def ::piece #{:dark :light})

(defn neighbors [[x y]]
  (letfn [(n  [[x y]] [x (dec y) :n])
          (s  [[x y]] [x (inc y) :s])
          (e  [[x y]] [(inc x) y :e])
          (w  [[x y]] [(dec x) y :w])
          (ne [[x y]] [(inc x) (dec y) :ne])
          (sw [[x y]] [(dec x) (inc y) :sw])
          (nw [[x y]] [(dec x) (dec y) :nw])
          (se [[x y]] [(inc x) (inc y) :se])]
    (->> ((juxt n e s w ne sw nw se) [x y])
         (remove (fn [[x y]] (or (neg? x) (neg? y) (> y 7) (> x 7))))
         (reduce (fn [neighbs [x y dir]] (assoc neighbs dir [x y])) {}))))

(defn build-board []
  (->> (for [y (range 8) x (range 8)] [x y])
       (reduce (fn [board loc] (assoc-in board [loc :neighbors] (neighbors loc))) {})))

(defn setup-game
  []
  (-> {:board (build-board) :player :dark}
      (update-in [:board [3 4]] assoc :piece :dark)
      (update-in [:board [3 3]] assoc :piece :light)
      (update-in [:board [4 3]] assoc :piece :dark)
      (update-in [:board [4 4]] assoc :piece :light)))

(defn occupied-neighbors
  "Given a state and a location, return the direction of occupied neighbors.

  ex: #{:s :se :e}.
  "
  [state loc]
  (->> (get-in state [:board loc :neighbors])
       (map (fn [[dir neighb-loc]] [dir (get-in state [:board neighb-loc])]))
       (filter (fn [[dir neighb]] (:piece neighb)))
       (map first)
       (set)))

(defn traverse-direction
  "Traverse from a given location in the given direction until another location occupied
  by the current player's piece is found. Returns the path up to that piece (not
  including it) or nil if no such path is found."
  [state loc direction]
  (loop [path '()
         next-loc (get-in state [:board loc :neighbors direction])]
    (let [piece (get-in state [:board next-loc :piece])]
      (if (= piece (:player state))
        (not-empty path)
        (recur (cons next-loc path) (get-in state [:board next-loc :neighbors direction]))))))

(defn other-player [player]
  (if (= player :light) :dark :light))

(defn flippable-pieces
  [{:keys [player board] :as state} loc]
  (->> (occupied-neighbors state loc)
       (mapcat (partial traverse-direction state loc))))

(comment
  (def s0 (setup-game))
  (:player s0)
  :dark
  (select-keys (neighbors [2 3]) (occupied-neighbors s0 [2 3]))
  {:e [3 3], :se [3 4]}
  (traverse-direction s0 [2 3] :se)
  nil
  (traverse-direction s0 [2 3] :e)
  ([3 3])
  (flippable-pieces s0 [2 3])
  ([3 3])
  )

(defn validate-placement
  [{:keys [board player] :as state} piece loc]
  (let [target (get board loc)
        flips  (flippable-pieces state loc)]
    (-> state
        (cond-> (not target) (update :error conj :invalid-location))
        (cond-> (:piece target) (update :error conj :location-already-occupied))
        (cond-> (not= player piece) (update :error conj :piece-does-not-match-player))
        (cond-> (zero? (count flips)) (update :error conj :must-flip-at-least-one-piece))
        (cond-> (pos? (count flips)) (assoc :flips flips)))))

(defn add-piece [{:keys [board player] :as state} piece loc]
  (let [{:keys [flips error] :as validated-state} (validate-placement state piece loc)]
    (if error
      validated-state
      (let [next-board (reduce (fn [b flip] (assoc-in b [flip :piece] piece)) board flips)]
        (-> state
            (assoc :board next-board)
            (update-in [:board loc] assoc :piece piece)
            (assoc :player (other-player player)))))))

(defn display [state]
  (let [lines (->> (for [y (range 8) x (range 8)] (get-in state [:board [x y]]))
                   (map :piece)
                   (map #(cond (= % :dark) "X"
                               (= % :light) "O"
                               :else "."))
                   (partition 8)
                   (map (partial reduce str)))]

    (println (reduce str "  " (range 8)))
    (doseq [[y line] (map-indexed vector lines)]
      (println y line))
    (println (:player state))
    (when (:error state) (println (:error state)))
    state))



(comment
  (display (setup-game))

  (-> (setup-game)
      (display)
      (add-piece :dark [2 3])
      (display))



  (get-in (setup-game) [:board [3 3]])
  {:neighbors {:n [3 2], :e [4 3], :s [3 4], :w [2 3], :ne [4 2], :sw [2 4], :nw [2 2], :se [4 4]}, :piece :light}
  {:piece :light, :n [3 2], :w [2 3], :e [4 3], :s [3 4], :se [4 4], :sw [2 4], :ne [4 2], :nw [2 2]}

  )
