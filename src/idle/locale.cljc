(ns idle.locale
  (:require [taoensso.tempura :as tempura]))

(def dictionary
  {:en
   {:missing "[[MISSING TRANSLATION]]"
    :label
    {:email "email"
     :password "password"
     :confirm-password "confirm password"
     :new-password "new password"
     }
    :validation
    {:unauthorized "unauthorized"
     }
    :action
    {:sign-in "sign in"
     :sign-out "sign out"
     :register "register"
     :email-me "email me"
     :reset-password "rest password"
     :confirm-reset "confirm reset"
     }
    :heist
    {:bear
     {:descriptor
      {:rookie "Rookie"
       :washed-up "Washed-up"
       :retired "Retired"
       :unhinged "Unhinged"
       :slick "Slick"
       :incompetent "Incompetent"}
      :type
      {:grizzly "Grizzly Bear"
       :polar "Polar Bear"
       :panda "Panda Bear"
       :black "Black Bear"
       :sun "Sun Bear"
       :honey-badger "Honey Badger"}
      :skill
      {:terrify "Terrify"
       :swim "Swim"
       :eat-anything-that-looks-like-bamboo "Eat anything that looks like bamboo"
       :climb "Climb"
       :sense-honey "Sense honey"
       :carnage "Carnage"}
      :role
      {:muscle "Muscle"
       :brains "Brains"
       :driver "Driver"
       :hacker "Hacker"
       :thief "Thief"
       :face "Face"}}}
    }})

(def t (partial tempura/tr {:dict dictionary} [:en]))
