(ns idle.boggle.subscriptions
  (:require [re-frame.core :as rf]
            [idle.boggle.game :as boggle-game]))

(rf/reg-sub
 ::board
 (fn [db _]
   (:board db)))

(rf/reg-sub
 ::words
 (fn [db _]
   (reverse (:words db))))

(rf/reg-sub
 ::candidate
 (fn [db _]
   (.toLowerCase (:candidate db))))

(rf/reg-sub
 ::error
 (fn [db _]
   (:error db)))

(rf/reg-sub
 ::score
 (fn [db _]
   (reduce (fn [score word] (+ score (boggle-game/word-score word)))
           0
           (:words db))))

(rf/reg-sub
 ::timer
 (fn [db _]
   (:timer db)))

(rf/reg-sub
 ::time-up
 (fn [db _]
   (zero? (:timer db))))

(rf/reg-sub
 ::selected
 (fn [db [_ coord]]
   (contains? (reduce #(clojure.set/union %1 (set %2)) #{} (:paths db)) coord)))

(rf/reg-sub
 ::letter
 (fn [db [_ [x y]]]
   (get-in db [:board y x])))

(rf/reg-sub
 ::previous-letter-coord
 (fn [db [_ coord]]
   (last (take-while #(not= % coord) (first (:paths db))))))

(comment
  (def p1 #{[[2 2]]})
  (def p2 #{[[2 2] [2 3] [3 2] [2 1]]})
  (def p3 #{[[0 1]]})

  (let [coord [0 1]]
    (last (take-while #(not= % coord) (first p3))))
  nil
  [0 1]

  (map (last (take-while #(not= [2 1] %) (first p2))))
  [3 2]


  ()

  (first (map #(last (butlast %)) p1))
  nil
  ([3 2])

  (let [x1 1 x2 nil]
    )
  (- 1 nil)1
  (- nil 1)-1
  (and nil nil 1)
  )
