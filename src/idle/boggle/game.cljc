(ns idle.boggle.game
  (:require [clojure.set :as set]
            [idle.boggle.data :as boggle-data]))

(defn validate-word
  [{:keys [words candidate] :as state}]
  (let [word (.toUpperCase candidate)
        long-enough? (>= (count word) 3)
        unique? (not ((set words) word))]
    (-> state
        (cond-> (not long-enough?)
          (update :error conj :word-must-have-at-least-three-letters))
        (cond-> (not unique?)
          (update :error conj :no-repeat-words))
        (cond-> (and long-enough? unique?)
          (assoc :word word)))))

(defn word-score [word]
  (if (< (count word) 5)
    1
    (count (drop 3 word))))

(defn neighbors [[x y]]
  (letfn [(n  [[x y]] [x (dec y)])
          (s  [[x y]] [x (inc y)])
          (e  [[x y]] [(inc x) y])
          (w  [[x y]] [(dec x) y])
          (ne [[x y]] [(inc x) (dec y)])
          (sw [[x y]] [(dec x) (inc y)])
          (nw [[x y]] [(dec x) (dec y)])
          (se [[x y]] [(inc x) (inc y)])]
    (->> ((juxt n e s w ne sw nw se) [x y])
         (remove (fn [[x y]] (or (neg? x) (neg? y) (> y 3) (> x 3))))
         (set))))

(defn neighb-dir
  "Returns the relative direction of coord2 to coord1."
  [[x1 y1] [x2 y2]]
  (and x1 y1 x2 y2
       (get {[0 -1] "n"
             [0 1] "s"
             [1 0] "w"
             [-1 0] "e"
             [-1 -1] "nw"
             [-1 1] "sw"
             [1 1] "se"
             [1 -1] "ne"}
            [(- x1 x2) (- y1 y2)])))

(comment
  (neighbors [2 2])
  #{[2 1] [3 2] [2 3] [1 2] [3 1] [1 3] [1 1] [3 3]}
  (map #(vector (neighb-dir [2 2] %) %) (neighbors [2 2]))
  ([:n [2 1]] [:e [3 2]] [:s [2 3]] [:w [1 2]] [:ne [3 1]] [:sw [1 3]] [:nw [1 1]] [:se [3 3]])
  ([:n [2 1]]
   [:e [3 2]]
   [:s [2 3]]
   [:w [1 2]]
   [:ne [3 1]]
   [:sw [1 3]]
   [:nw [1 1]]
   [:se [3 3]])


  (neighb-dir [2 2] [0 0])


  )

(defn letter-coords
  "Given a board and a letter, return all the x,y coordinates where the letter appears on
  the board. Returns an empty set if the letter doesn't appear on the board.

  Special treatment for Q: if we pass Q, look for Qu. This will allow us to not throw an
  error while someone is mid-typing."
  [board letter]
  (let [normalized-letter (if (= letter "Q") "Qu" letter)]
    (loop [y 0
           coords #{}]
      (if (= y 4)
        (set coords)
        (let [new-coords (reduce (fn [matches x]
                                   (if (= (get-in board [y x]) normalized-letter)
                                     (conj matches [x y])
                                     matches))
                                 #{}
                                 (range 4))]
          (recur (inc y) (set/union coords new-coords)))))))

(defn create-game
  ([]
   (create-game (boggle-data/gen-board)))
  ([board]
   {:board board                        ; a vector of 4 4-element vectors
    :candidate ""
    :paths nil
    :words []}))                        ; a vector of words

(defn valid-neighbor?
  "Valid if the letter is an untraversed neighbor to a path's tail."
  [path letter]
  (contains? (set/difference (neighbors (last path))
                             (set path))
             letter))

(defn new-paths
  "Returns a set of new paths through the board with the new letter's locations."
  [paths letter-locs]
  (set (mapcat (fn [path] (->> letter-locs
                               (map #(if (valid-neighbor? path %)
                                       (conj path %)
                                       path))
                               ;; remove all paths that aren't the right length
                               (filter #(> (count %) (count path)))))
               paths)))

;; could do coordinate validation only (click on letter)
(defn add-coordinate
  [{:keys [board candidate paths] :as state} letter-coord]
  (let [[x y]       letter-coord
        letter-locs #{letter-coord}
        letter      (get-in board [y x])
        next-paths  (new-paths paths letter-locs)]
    (cond (nil? paths)
          (-> state
              (assoc :paths (set (map vector letter-locs)))
              (assoc :candidate (str candidate letter)))
          (not-empty next-paths)
          (-> state
              (assoc :paths next-paths)
              (assoc :candidate (str candidate letter)))
          :else
          (-> state
              (assoc :error :invalid-letter)))))

(defn boggle-letters
  "Given a string candidate, separate it into its component characters, glomming
  together 'Q' and 'U'. Returns a sequence of letters."
  [candidate]
  (->> (partition-all 2 1 candidate)
       (reduce (fn [res [a b]]
                 (cond (and (= a "Q")
                            (not b))
                       (conj res "Q")
                       (and (= a "Q")
                            (= (.toUpperCase b) "U"))
                       (conj res "Qu")
                       (and (= (last res) "Qu")
                            (= (.toUpperCase a) "U"))
                       res
                       :else
                       (conj res a)))
               [])))

(defn insert-u-after-q
  "Insert a 'U' after a 'Q' if it's missing."
  [candidate]
  (->> (partition-all 2 1 candidate)
       (reduce (fn [res [a b]]
                 (cond (and (= a "Q")
                            (not= b "U"))
                       (str res "QU")
                       (and (= (last res) "QU")
                            (= a "U"))
                       res
                       :else
                       (str res a)))
               "")))

(defn board->url [board]
  (->> board
       (reduce (fn [url row] (str url (reduce str row))) "")
       (boggle-letters)
       (map #(if (= "Qu" %) "Q" %))
       (reduce str)
       (.toLowerCase)))

(defn url->board [url]
  (when-let [url-param (and url (.toUpperCase url))]
    (let [board-string (boggle-letters (insert-u-after-q url-param))]
      (when (= (count board-string) 16)
        (mapv vec (partition 4  board-string))))))

(comment
  (let [board [["V" "O" "Qu" "N"]
               ["E" "U" "E" "M"]
               ["W" "C" "E" "W"]
               ["T" "I" "R" "A"]]
        url (board->url board)]
    (= (url->board url) board))
  true)

(defn update-word
  "When typing we don't know whether the next submission will have a letter at the end or
  in the middle or whether a letter is getting removed. So we re-validate the whole
  candidate.

  The string must always be updated, never prevent a user from typing."
  [{:keys [board] :as state} candidate]
  (if (= "" candidate)
    ;; reset the error state
    (-> state
        (assoc :candidate "")
        (dissoc :error :paths))
    (let [candidate-letters (boggle-letters candidate)]
      (if (and ((set candidate-letters) "Q")
               ;; FIXME: this is problematic, I think
               (not= "Q" (last candidate-letters)))
        ;; If we have a Q that's not followed by a U
        (-> state
            (assoc :candidate (reduce str candidate-letters))
            (update :error conj [:invalid-letter :q-unfollowed-by-u "Q"]))
        ;; Validate the whole word
        (reduce
         (fn [{:keys [paths] :as state} letter]
           (let [letter-locs (letter-coords board letter)
                 next-paths (new-paths paths letter-locs)
                 next-state (update state :candidate str letter)]
             ;; permit invalid letters in candidate, but indicate there is a problem
             (cond  (empty? letter-locs) ; letter is not on the board
                    (-> next-state
                        (assoc paths next-paths)
                        (update :error conj [:invalid-letter :not-on-board letter]))
                    (nil? paths)        ; first letter
                    (-> next-state
                        (assoc :paths (set (map vector letter-locs))))
                    (not-empty next-paths) ; valid letter
                    (-> next-state
                        (assoc :paths next-paths))
                    :else               ; some other problem
                    (-> next-state
                        (update :error conj [:invalid-letter letter])
                        (assoc :paths next-paths)))))
         {:paths nil :candidate ""}        ; <-- key bit, start from scratch each time
         candidate-letters)))))


(comment
  (letter-coords [["Qu" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]] "QA")
  #{}
  #{[0 0]}

  (update-word {:board [["Qu" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]]
                :paths nil :string ""} "OQA")


  (update-word {:board b :paths nil :string ""} "W")
)

(defn add-word [{:keys [candidate words] :as state}]
  (let [word (reduce str candidate)]
    (if (>= (count word) 3)
      (-> state
          (assoc :words (conj words word))
          (assoc :candidate "")
          (assoc :paths nil))
      (-> state
          (assoc :error :word-must-have-at-least-three-letters)))))

(comment
  ;; display a board
  (gen-board)

  ;; add a letter
  ;; validate it
  ;; ;; make sure it is on the board
  ;; ;; make sure it is adjacent to the last letter (if it's not the first)
  ;; ;; make sure it isn't a repeat

  ;; submit a word
  ;; validate it
  ;; ;; make sure it's at least 3 letters long
  ;; ;; make sure it's a dictionary word?

  (def b [["V" "O" "E" "N"]
          ["E" "U" "E" "M"]
          ["W" "C" "E" "W"]
          ["T" "I" "R" "A"]])


  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W"],
   :paths {[0 2] [[0 2]], [3 2] [[3 2]]}}

  (create-game b)
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate [],
   :paths nil,
   :words []}

  (-> (create-game b)
      (add-letter "W"))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W"],
   :paths #{[[3 2]] [[0 2]]},
   :words []}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E"))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W" "E"],
   :paths ([[3 2] [2 2]] [[3 2] [2 1]] [[0 2] [0 1]]),
   :words []}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E")
      (add-letter "A"))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W" "E" "A"],
   :paths #{[[3 2] [2 2] [3 3]]},
   :words []}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E")
      (add-letter "A")
      (add-letter "R"))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate ["W" "E" "A" "R"],
   :paths #{[[3 2] [2 2] [3 3] [2 3]]},
   :words []}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E")
      (add-letter "A")
      (add-letter "R")
      (add-word))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate [],
   :paths nil,
   :words ["WEAR"]}

  (-> (create-game b)
      (add-letter "W")
      (add-letter "E")
      (add-letter "A")
      (add-letter "R")
      (add-word)
      (add-letter "C")
      (add-letter "R")
      (add-letter "E")
      (add-letter "W")
      (add-word))
  {:board [["V" "O" "E" "N"] ["E" "U" "E" "M"] ["W" "C" "E" "W"] ["T" "I" "R" "A"]],
   :candidate [],
   :paths nil,
   :words ["WEAR" "CREW"]}

  )
