(ns idle.boggle.views.home
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [idle.boggle.events :as evt]
            [idle.boggle.subscriptions :as sub]
            [idle.boggle.game :as boggle-game]))

(kf/reg-controller
 :route.boggle/home
 {:params (fn [route] (when (= (-> route :data :name) :route.boggle/home) true))
  :start [::evt/initialize-boggle]})

(defn boggle-page []
  (let [board (rf/subscribe [::sub/board])]
    [:div [:h2 "Boggle"]
     [:div.intro
      [:ul
       [:li "Find words"]
       [:li "Only consecutive adjacent letters"]
       [:li "No reusing letters"]
       [:li "3 letter minimum"]]
      [:a.new-game {:href (kf/path-for [:route.boggle/play {:board (boggle-game/board->url @board)}])}
       "Start"]]]))
