(ns idle.boggle.views.play
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [idle.boggle.events :as evt]
            [idle.boggle.subscriptions :as sub]
            [idle.boggle.game :as boggle-game]))

(defn draw-letter
  ([coord]
   (draw-letter coord 0))
  ([[x y] rotation]
   (let [letter   (rf/subscribe [::sub/letter [x y]])
         selected (rf/subscribe [::sub/selected [x y]])
         previous (rf/subscribe [::sub/previous-letter-coord [x y]])]
     (fn []
       [:div.letter {:class (when @selected "selected")}
        [:div.inner {:class (boggle-game/neighb-dir [x y] @previous)
                     :on-click #(rf/dispatch [::evt/add-letter [x y]])}
         [:span {:style (merge {:transform (str "rotate(" rotation "deg)")}
                               (when (#{"Z" "N" "W" "M"} @letter)
                                 {:text-decoration "underline"}))}
          @letter]]]))))

(defn rotated-letter
  "A helper function that separates the calculation of the rotation from the render function."
  [coord]
  (let [rotation (* 90 (rand-int 4))]
    (draw-letter coord rotation)))

(defn play-page []
  (let [words     (rf/subscribe [::sub/words])
        candidate (rf/subscribe [::sub/candidate])
        score     (rf/subscribe [::sub/score])
        error     (rf/subscribe [::sub/error])]
    (fn []
      [:div.boggle
       [:div.board
        (doall
         (for [y (range 4) x (range 4)] ^{:key [x y]}
           [rotated-letter [x y]]))]
       [:input#wordentry {:type      :text
                          :value     @candidate
                          :style     (when @error {:border "thick solid darkred"})
                          :auto-complete "off"
                          :spell-check false
                          :on-change #(rf/dispatch [::evt/update-word (.-value (.-target %))])}]
       [:div.buttons
        [:button {:on-click #(rf/dispatch [::evt/clear-candidate])}"CLEAR"]
        [:button {:on-click #(rf/dispatch [::evt/add-word])} "SUBMIT"]]
       [:div.progress
        [:div.score {:style {:align-items "center"}} @score]
        [:ul.wordlist
         (for [word @words] ^{:key word}
           [:li word])]]])))
