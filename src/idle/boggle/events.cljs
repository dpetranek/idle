(ns idle.boggle.events
  (:require [re-frame.core :as rf]
            [reagent.core :as r]
            [idle.common :as common]
            [idle.boggle.game :as boggle-game]))

(rf/reg-fx
 ::shake-element-by-id
 (fn [[el-id]]
   (let [el (.getElementById js/document el-id)]
     (set! (.-className el) "shake")
     (.setTimeout js/window #(set! (.-className el) "") 250))))

(rf/reg-fx
 ::timer
 (fn [[seconds]]
   (let [time (r/atom seconds)]
     (when (pos? @time)
       (println "updating time" @time)
       (js/setTimeout #(swap! time dec) 1000)
       (rf/dispatch [::update-timer @time])))))

(defn handle-enter-keypress [e]
  (when (= 13 (.-which e))
    (rf/dispatch [::add-word])))

(rf/reg-event-fx
 ::initialize-boggle
 [rf/debug]
 (fn [{:keys [db]} [_ board-string]]
   {:db (merge db (boggle-game/create-game))
    ::common/add-window-event-listener ["keydown" handle-enter-keypress]}))

(rf/reg-event-fx
 ::destroy-boggle
 [rf/debug]
 (fn [{:keys [db]} _]
   {::common/remove-window-event-listener ["keydown"  handle-enter-keypress]
    :db (dissoc db :timer :board :candidate :paths :words :error)}))

(rf/reg-event-fx
 ::start-timer
 [rf/debug]
 (fn [db [_ seconds]]
   {
    :db (assoc db :timer seconds)}))

(rf/reg-event-db
 ::update-timer
 (fn [db [_ seconds]]
   (assoc db :timer seconds)))

(rf/reg-event-db
 ::clear-candidate
 [rf/debug]
 (fn [db _]
   (-> db
       (assoc :candidate "")
       (dissoc :error :paths))))

(rf/reg-event-fx
 ::add-letter
 [rf/debug]
 (fn [{:keys [db]} [_ coord]]
   (let [{:keys [error paths candidate]} (boggle-game/add-coordinate db coord)
         touched-path (filter #(= (last %) coord) paths)]
     {:db (assoc db :error error :paths (set touched-path) :candidate candidate)})))

(rf/reg-event-fx
 ::update-word
 [rf/debug]
 (fn [{:keys [db]} [_ candidate]]
   (let [upcased-candidate (.toUpperCase candidate)
         {:keys [error paths candidate]} (boggle-game/update-word db upcased-candidate)]
     {:db (assoc db :error error :paths paths :candidate candidate)})))

(rf/reg-event-fx
 ::add-word
 [rf/debug]
 (fn [{:keys [db]} _]
   (let [time-up? (:time-up db)
         {:keys [error word]} (boggle-game/validate-word db)
         next-db (-> db
                     (assoc :candidate "")
                     (assoc :paths nil))]
     (cond error
           {:db (-> next-db (assoc :error error))
            :dispatch [::handle-invalid-word]}

           time-up?
           {:db db}

           :else
           {:db (-> next-db (update :words conj word))}))))

(rf/reg-event-fx
 ::handle-invalid-word
 [rf/debug]
 (fn [{:keys [db]} _]
   {:db (dissoc db :error)
    ::shake-element-by-id ["wordentry"]}))

(rf/reg-event-db
 ::new-game
 [rf/debug]
 (fn [_ _]
   (boggle-game/create-game)))
