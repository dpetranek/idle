(ns idle.boggle.schema
  (:require [clojure.spec.alpha :as s]))

(s/def ::id (s/and string?
                   #(or (= (count %) 16)
                        ;; For "Qu"
                        (= (count %) 17))))

(s/def ::word string?)
(s/def ::wordlist (s/coll-of ::word))

(s/def ::player-id uuid?)

;; entities: {:}
{:id "abcdefghijklmnop"}
{:player-id "abc123"
 :game "ref"
 :wordlist ["abc" "def" "ghi" "jklm" "nop"]}
(def boggle-schema
  [
   {:db/ident       :game/id
    :db/valueType   :db.type/string
    :db/unique      :db.unique/identity
    :db/cardinality :db.cardinality/one}

   {:db/ident       :game/id
    :db/valueType   :db.type/string
    :db/unique      :db.unique/identity
    :db/cardinality :db.cardinality/one}

   ])
