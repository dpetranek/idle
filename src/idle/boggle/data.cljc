(ns idle.boggle.data)

(def cubes
  [["T" "W" "A" "O" "T" "O"]
   ["R" "H" "Z" "L" "N" "N"]
   ["O" "T" "M" "I" "C" "U"]
   ["D" "T" "Y" "T" "I" "S"]
   ["R" "T" "T" "E" "L" "Y"]
   ["A" "G" "A" "E" "N" "E"]
   ["A" "S" "P" "F" "K" "F"]
   ["O" "A" "B" "B" "O" "J"]
   ["I" "S" "S" "O" "T" "E"]
   ["U" "N" "H" "I" "M" "Qu"]
   ["E" "D" "L" "X" "R" "I"]
   ["S" "P" "A" "H" "O" "C"]
   ["H" "E" "E" "N" "G" "W"]
   ["R" "E" "T" "W" "H" "V"]
   ["E" "V" "Y" "R" "D" "L"]
   ["S" "U" "E" "E" "N" "I"]])

(defn roll [cube]
  (nth cube (rand-int 6)))

(defn rotate [letter]
  [letter (* (rand-int 4) 90)])

(defn gen-board []
  (->> (shuffle cubes)
       (map roll)
       (partition 4)
       (mapv vec)))
