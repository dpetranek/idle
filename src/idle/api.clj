(ns idle.api
  (:require [reitit.ring :as ring]
            [taoensso.timbre :as timbre]
            [clojure.spec.alpha :as s]
            [ring.middleware.resource :as rmr]
            [reitit.ring.coercion :as coercion]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.parameters :as parameters]
            [muuntaja.core :as m]
            [idle.middleware :as middleware]
            [idle.handlers :as handlers]))

(def router
  (ring/router
   [["/ws" handlers/ws-handler]
    ["/api"
     ["/command" {:post {:handler handlers/command-handler}}]
     ["/query" {:post {:handler handlers/query-handler}}]]]
   {:data {:muuntaja m/instance
           :middleware [[middleware/wrap-exception :exception]
                        ;; query-params & form-params
                        parameters/parameters-middleware
                        ;; content-negotiation
                        muuntaja/format-negotiate-middleware
                        ;; encoding response body
                        muuntaja/format-response-middleware
                        ;; decoding request body
                        muuntaja/format-request-middleware
                        ;; coercing response bodys
                        coercion/coerce-response-middleware
                        ;; coercing request parameters
                        coercion/coerce-request-middleware
                        ;; log request and response
                        [middleware/wrap-logging :logging]

                        ] }}))

(def app
  (-> (ring/ring-handler
       router
       (fn [_] {:status 200 :body (slurp "resources/public/index.html")}))
      (rmr/wrap-resource "public")))

(comment
  (app {:uri "/" :request-method :get})
  (app {:uri "/boggle" :request-method :get})
  (app {:uri "/foo" :request-method :get})

  (app {:uri "/api/command" :request-method :post :body {:cmd "sign-in" :payload {:username "asdf" :password "fdsa"}}})


  )
