(ns idle.validators)

;; A validator takes a payload and a context and returns nil if everything is good
;; it will determine if the user is authorized to perform a command and if the command is
;; well formed

(defmulti validate (fn [cmd context] cmd))

(defn field-error
  [errors field err]
  (assoc-in errors [:errors 400 field] err))

(defn auth-error
  [errors cmd]
  (assoc-in errors [:errors 403 cmd] :not-authorized))
