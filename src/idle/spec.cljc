(ns idle.spec
  (:require [clojure.spec.alpha :as s]))

;; the data from the client
(s/def ::params map?)
;; map of param keys to error keys
(s/def ::errors map?)
(s/def ::validation (s/keys :req-un [::params ::errors]))

;; I should build an effect registry so I can reify in a more organized way
(s/def ::effects (s/coll-of map?))
;; the data to return to the client
(s/def ::result map?)
(s/def ::handler-result (s/keys :req-un [::params ::errors ::effects ::result]))

;; http status code and body to be jsonized
(s/def ::response (s/keys :req-un [::status ::body]))


(s/def ::handlers (s/keys :opt-un [::validator ::payload ::success ::failure]))
(s/def ::form (s/keys :req-un [::form-id ::fields ::cmd]
                      :opt-un [::handlers]))
