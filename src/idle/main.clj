(ns idle.main
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]
            [juxt.clip.core :as clip])
  (:gen-class))

(def system nil)

(defn system-config
  [profile]
  (edn/read-string (slurp (io/resource (str "system_" (name profile) ".edn")))))

(defn -main
  [& args]
  (let [system (clip/start (system-config :prod))]
    (alter-var-root #'system (constantly system))
    (.addShutdownHook
     (Runtime/getRuntime)
     (Thread. #(clip/stop system-config system)))
    @(promise)))
