(ns idle.handlers
  (:require [buddy.hashers :as hash]
            [datahike.api :as d]
            [immutant.web.async :as iwa]
            [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [taoensso.timbre :as timbre]
            [idle.validators :as v]
            [idle.heist.data :as heist.data]
            [clojure.set :as set]
            [clojure.edn :as edn]))

(defonce ws-channels (atom {}))

(def ^{:private true} db nil)

(defn set-db
  "This is a hack so I don't have to move all of this namespace into a config file."
  [db]
  (alter-var-root #'db (constantly db)))

(defmulti process
  "Takes a context (:user, :db, :payload) and returns a context with {:errors :response :effects}."
  (fn [context]
    (timbre/info "process" (pr-str (:params context)))
    (:cmd (:params context))))
(defmethod process :default [context] context)

(defmulti effect
  "Takes an effect (from a vector of effects) and executes it."
  (fn [effect transaction context] (timbre/info "effect" (pr-str effect)) effect))

(defmethod effect :db
  [_ transaction context] (d/transact (:db context) transaction))

(defmethod effect :ws
  [_ recipient-ids context]
  (let [channels (map (fn [id] (get @ws-channels id)) recipient-ids)]
    (doseq [channel channels]
      (iwa/send! channel (pr-str (:response context))))))

(defmethod v/validate :cmd/register
  [{:keys [payload db]}]
  (let [{:keys [username password password2]} payload
        pre-existing-user (into {} (d/entity @db [:user/username username]))]
    (-> {}
        (cond-> (str/blank? username)  (v/field-error :username :required))
        (cond-> (str/blank? password)  (v/field-error :password :required))
        (cond-> (str/blank? password2) (v/field-error :password2 :required))
        (cond-> (< (count password) 8) (v/field-error :password :insufficient-length))
        (cond-> (not= password password2) (v/field-error :password2 :passwords-must-match))
        (cond-> (= username (:user/username pre-existing-user))
          (v/field-error :username :already-registered-username))
        (not-empty))))

(defmethod process :cmd/register
  [{:as context :keys [payload]}]
  (let [{:keys [username password]} payload
        {:keys [errors]} (v/validate :cmd/register context)
        user-id (java.util.UUID/randomUUID)
        token (java.util.UUID/randomUUID)]
    (merge context
           {:errors errors
            :response {:status 200 :token token :username username :id user-id}
            :effects {:db [{:user/username username
                            :user/id user-id
                            :user/pass-hash (hash/derive password)
                            :user/session-token token}]}})))

(defmethod v/validate :cmd/sign-in
  [_ {:keys [payload user]}]
  (let [{:keys [username password]} payload]
    (-> {}
        (cond-> (str/blank? username) (v/field-error :username :required))
        (cond-> (str/blank? password) (v/field-error :password :required))
        (cond-> (not (hash/check password (:user/pass-hash user)))
          (v/field-error :password :incorrect-password))
        (cond-> (not= username (:user/username user))
          (v/field-error :username :no-account-found-for-username))
        (not-empty))))

(defmethod process :cmd/sign-in
  [{:as context :keys [payload db]}]
  (let [username (:username payload)
        {:as user :user/keys [id]} (into {} (d/entity @db [:user/username username]))
        {:keys [errors]} (v/validate :cmd/sign-in (assoc context :user user))
        token (java.util.UUID/randomUUID)]
    (merge context
           {:errors errors
            :response {:token token :username username :id id}
            :effects {:db [{:user/id id :user/session-token token}]}})))

(defmethod process :cmd/sign-out
  [{:as context :keys [user]}]
  (let [errors (when-not user (v/auth-error {} :cmd/sign-out))
        token (:user/token user)]
    (merge context
           {:errors errors
            :response {}
            :effects {:db [[:db/retract [:user/session-token token] :session-token token]]}})))

(defmethod v/validate :cmd/create-heist
  [_ {:keys [payload]}]
  (-> {}
      (cond-> (not (s/valid? ::heist.data/heist payload))
        (v/field-error :selected :invalid-scenario))
      (not-empty)))

(defmethod process :cmd/create-heist
  [{:as context :keys [user payload]}]
  (let [{:keys [errors]} (v/validate :cmd/create-heist context)
        heist-id (java.util.UUID/randomUUID)]
    (merge context
           {:errors errors
            :response {:id heist-id}
            :effects {:db [(merge payload
                                  {:heist/id heist-id
                                   :db/id -1
                                   :heist/manager [:user/id (:user/id user)]})
                           {:db/id [:user/id (:user/id user)]
                            :user/game -1}]}})))

(defmethod v/validate :cmd/join-game
  [_ {:keys [payload heist]}]
  (let []
    (-> {}
        (cond-> (not (s/valid? ::heist.data/bear payload))
          (v/field-error :selected :invalid-bear))
        (cond-> (empty? heist)
          (assoc-in [:errors :bear/heist] :nonexistent-heist))
        (not-empty))))

(defmethod process :cmd/join-game
  [{:as context :keys [user payload db]}]
  (let [heist (into {} (d/entity @db [:heist/id (:bear/heist payload)]))
        {:keys [errors]} (v/validate :cmd/join-game (assoc context :heist heist))
        bear-id (java.util.UUID/randomUUID)]
    (merge context
           {:errors errors
            :response {:id bear-id}
            :effects {:db [
                           ;; create the bear
                           (merge payload
                                  {:db/id -1 :bear/id bear-id
                                   :bear/heist [:heist/id (:heist/id heist)] ; bear->heist
                                   :bear/player [:user/id (:user/id user)] }) ; bear->user
                           {:db/id [:heist/id (:heist/id heist)]
                            :heist/bear -1} ; heist->bear
                           {:db/id [:user/id (:user/id user)]
                            :user/game [:heist/id (:heist/id heist)] ; user->bear
                            :user/character -1}]}})))

(defmethod process :cmd/shift-stat
  [{:as context :keys [user payload db]}]
  (let [bear-id (:bear/id payload)
        stat    (:stat payload)

        {bear-stat              :bear/bear
         criminal-stat          :bear/criminal
         {{manager-id :user/id} :heist/manager
          heist-id              :heist/id
          players               :heist/bear} :bear/heist}

        (d/pull @db [:bear/bear :bear/criminal {:bear/heist [:heist/id {:heist/manager [:user/id]} {:heist/bear [:user/id]}]}]
                [:bear/id bear-id])

        recipients (conj (set (map (comp :user/id :bear/player) players)) manager-id)]
    (merge context
           {
            ;; I can either just trigger a reload on the client or send the specific info to be patched in.
            ;; I'm going to just trigger a reload.
            :response {:dispatch
                       [:idle.common/resolve-data :route.heist/play
                        [:heist/id heist-id]
                        ["*" {:heist/manager ["*"]} {:heist/bear ["*" {:bear/player ["*"]}]}]]}
            :effects  {:ws recipients
                       ;; TODO: consider using cas here
                       ;; TODO: don't increment beyond 6, decrement beyond 0
                       :db [{:db/id         [:bear/id bear-id]
                             :bear/bear     ((if (= stat :bear) inc dec) bear-stat)
                             :bear/criminal ((if (= stat :criminal) inc dec) criminal-stat)}]}})))

;; in order to process, we need to take the following steps:
;; 1. authorization - attach a :user key to the context if the request initiator is authenticated
;; 2. payload - a payload is a specific param that contains the actual action to be processed
;; 3. inject dependencies - attach the :db to the context, perhaps other dependencies for processing
;; then we'll process the request, create a response, and create some effects
;; then we'll execute the effects and return the response.

(def private-attrs #{:user/session-token :user/pass-hash})

(defn get-selection-attrs
  "Flatten a selection. Not stack safe."
  [selection]
  (loop [[v & rest] selection
         res      []]
    (cond (coll? v) (recur rest (concat res (get-selection-attrs (seq v))))
          (nil? v)  (set res)
          :else     (recur rest (conj res v)))))

(defn authorized-selection?
  "Check to see if a query uses any private attrs, and reject it if it does."
  [selection]
  (empty? (set/intersection private-attrs (get-selection-attrs selection))))

(defn query-handler [req]
  (let [{:keys [query selection]} (:body-params req)
        response (d/pull @db selection query)]
    (if (authorized-selection? selection)
      {:status 200 :body response}
      {:status 403 :body {:error :query-contains-private-attributes}})))

(defn err-handler
  "Takes a map of errors and returns the largest status code and the accompanying error
  body."
  [errors]
  (let [[status err] (last (sort errors))]
    {:status status :body err}))

(defn command-handler [req]
  (let [{{token :token :as params} :body-params} req
        ;; look up the user, if the token is valid
        user (into {} (d/entity @db [:user/session-token token]))
        ;; process result
        {:as context :keys [errors effects response]}
        (process {:request req
                  :params params
                  :payload (:payload params)
                  :user user
                  :db db})]
    (when-not errors
      (doseq [[fx transaction] effects]
        (effect fx transaction context)))
    (if errors
      (err-handler errors)
      {:status 200 :body response})))

(defn msg-handler [channel msg]
  (let [msg (edn/read-string msg)
        {token :token :as params} msg
        {:as user :user/keys [id]} (into {} (d/entity @db [:user/session-token token]))
        {:as context :keys [errors effects]} (process {:params params
                                                       :payload (:payload params)
                                                       :user user
                                                       :db db})]
    (timbre/info "ws-msg" (pr-str msg))
    (when id
      (swap! ws-channels assoc id channel channel id))
    (when-not errors
      (doseq [[fx transaction] effects]
        (effect fx transaction context)))))

(count @ws-channels)


(defn ws-handler [req]
  (iwa/as-channel req {:on-open (fn connect! [channel]
                                  (timbre/info "ws-connect"))
                       :on-close (fn disconnect! [channel {:keys [code reason]}]
                                   (let [user-id (get @ws-channels channel)]
                                     (timbre/info "ws-disconnect" user-id)
                                     (swap! ws-channels dissoc channel user-id)))
                       :on-message msg-handler}))
