(ns idle.home.views.layout
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [idle.common :as common]
            [idle.forms :as form]
            [idle.locale :as i18n]
            [idle.home.events :as evt]))

(def sign-in-form-id ::sign-in-form)

(defn validate-sign-in-form [{:keys [username password]}]
  (-> {}
      (cond-> (empty? username) (assoc :username :required))
      (cond-> (empty? password) (assoc :password :required))
      (not-empty)))

(rf/reg-event-fx
 ::validate-sign-in-form
 [rf/debug]
 (fn [{:keys [db]} [_ form-id]]
   (let [form-values (get-in db [form-id :values])]
     (if-let [errors (validate-sign-in-form form-values)]
       {:db (assoc-in db [form-id :errors] errors)}
       {:db (update db form-id dissoc :errors)
        :dispatch [:form/submit form-id]}))))

(defn sign-in-form []
  ;; TODO: This is bad form, there's probably a better way
  (rf/dispatch [:form/register-form {:form-id sign-in-form-id
                                     :cmd :cmd/sign-in
                                     :fields {:username ""
                                              :password ""}
                                     :handlers {:validator ::validate-sign-in-form
                                                :success ::evt/auth-success}}])
  ;; TODO I don't like the preventDefault here, find alternative?
  [:form {:on-submit #(do (.preventDefault %)
                          (rf/dispatch [:form/start-submit sign-in-form-id]))}
   [:label {:for "#username"} "username"]
   [form/field {:id :username :type :text :name :username :form-id sign-in-form-id}]
   [:label {:for "#password"} "password"]
   [form/field {:id :password :type :password :name :password :form-id sign-in-form-id}]
   [:button {:type :submit} "sign in"]
   [:div.row {:style {:justify-content :space-between}}
    [:a {:href (kf/path-for [:route/reset])
         :style {:text-align :right}} "reset password"]
    [:a {:href (kf/path-for [:route/register])
         :style {:text-align :right}} (i18n/t [:action/register])]]])

(defn sidebar []
  (let [token (rf/subscribe [::common/token])
        username (rf/subscribe [::common/username])
        user-id (rf/subscribe [::common/user-id])
        route-name (rf/subscribe [::common/route-name])]
    (fn []

      [:div.sidebar
       [:div.logo [:a {:href (kf/path-for [:route/home])} "I"]]
       (when @token
         [:div [:a {:href (kf/path-for [:route.user/home {:user-id @user-id}])} @username]
          [:button {:on-click #(rf/dispatch [::evt/sign-out])} "sign out"]])
       (when (and (not @token) (not= @route-name :route/register))
         [sign-in-form])
       [:button {:on-click #(rf/dispatch [::common/toggle-dark-mode])} "dark mode"]])))

(defn footer []
  [:footer])
