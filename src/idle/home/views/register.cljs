(ns idle.home.views.register
  (:require [re-frame.core :as rf]
            [kee-frame.core :as kf]
            [clojure.string :as str]
            [idle.forms :as form]
            [idle.home.events :as evt]))

(def register-form-id ::register-form)

(kf/reg-controller
 :route/register
 {:params (fn [route] (when (= (-> route :data :name) :route/register) true))
  :start [:form/register-form {:form-id register-form-id
                               :cmd :cmd/register
                               :fields {:username ""
                                        :password ""
                                        :password2 ""}
                               :handlers {:validator ::validate-register-form
                                          :success ::evt/auth-success}}]
  :stop [:form/unregister-form register-form-id]})

(defn validate-register-form [{:keys [username password password2]}]
  (-> {}
      (cond-> (str/blank? username) (assoc :username :required))
      (cond-> (str/blank? password) (assoc :password :required))
      (cond-> (str/blank? password2) (assoc :password2 :required))
      (cond-> (< (count password) 8) (assoc :password :insufficient-length))
      (cond-> (not= password password2) (assoc :password2 :passwords-must-match))
      (not-empty)))

(rf/reg-event-fx
 ::validate-register-form
 [rf/debug]
 (fn [{:keys [db]} [_ form-id]]
   (let [form-values (get-in db [form-id :values])]
     (if-let [errors (validate-register-form form-values)]
       {:db (assoc-in db [form-id :errors] errors)}
       {:db (update db form-id dissoc :errors)
        :dispatch [:form/submit form-id]}))))

(defn register-form []
  [:form {:on-submit #(do (.preventDefault %)
                          (rf/dispatch [:form/start-submit register-form-id]))}
   [:label {:for "#username"} "username"]
   [form/field {:id :username :type :text :name :username :form-id register-form-id}]
   [:label {:for "#password"} "password"]
   [form/field {:id :password :type :password :name :password :form-id register-form-id}]
   [:label {:for "#password2"} "confirm password"]
   [form/field {:id :password2 :type :password :name :password2 :form-id register-form-id}]
   [:button {:type :submit} "register"]])

(defn register-page []
  [:div [:h2 "Register"]
   [register-form]])
