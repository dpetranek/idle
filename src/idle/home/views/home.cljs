(ns idle.home.views.home
  (:require [kee-frame.core :as kf]))

(defn home-page []
  [:div [:h1 "Idle"]
   [:ul
    [:li [:a {:href (kf/path-for [:route.boggle/home])} "Boggle"]]
    [:li [:a {:href (kf/path-for [:route.heist/home])} "Honey Heist"]]
    [:li [:a {:href (kf/path-for [:route.masks/home])} "Masks"]]
    [:li "Hanoi"]
    [:li "Push Fight"]
    [:li "Othello"]]])
