(ns idle.home.events
  (:require [re-frame.core :as rf]
            [idle.common :as common]))

(rf/reg-event-fx
 ::sign-out
 [rf/debug]
 (fn [{:keys [db]} _]
   {:db db
    :dispatch [::common/request {:method :post
                                 :uri "/api/command"
                                 :params {:cmd :cmd/sign-out}
                                 :on-success [::sign-out-success]}]}))

(rf/reg-event-fx
 ::sign-out-success
 [rf/debug]
 (fn [{:keys [db]} _]
   {:db (dissoc db :user)
    ::common/remove-local-storage ["user"]
    :navigate-to [:route/home]}))

(rf/reg-event-fx
 ::auth-success
 [rf/debug]
 (fn [{:keys [db]} [_ {:keys [token username id] :as resp}]]
   {:db (-> db (assoc :user {:token token :username username :id id}))
    ::common/set-local-storage ["user" (pr-str resp)]
    :dispatch [::common/send-ws-msg {:cmd :cmd/identify}]
    :navigate-to [:route/home]}))
