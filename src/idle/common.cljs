(ns idle.common
  (:require [re-frame.interceptor :as rf-interceptor]
            [re-frame.core :as rf]
            [idle.websocket :as websocket]
            [idle.constants :as const]
            [ajax.core :as ajax]))

(def debug
  (rf-interceptor/->interceptor
   :id     :idle-debug
   :before (fn debug-before
             [context]
             (println "Handling event:" (get-in context [:coeffects :event]))
             context)
   :after  (fn debug-after
             [context]
             (let [event   (get-in context [:coeffects :event])
                   orig-db (get-in context [:coeffects :db])
                   new-db  (get-in context [:effects :db] ::not-found)]
               (tap> {:event  (get-in context [:coeffects :event])
                      :before orig-db
                      :after  new-db})
               (if (= new-db ::not-found)
                 (println :log "No :db changes caused by:" event)
                 (do (println "before" orig-db)
                     (println "after" new-db)))
               context))))

(rf/reg-event-fx
 ::request
 [rf/debug]
 (fn [{:keys [db]} [_ {:as request :keys [uri method params on-success on-failure]}]]
   (let [token (:token (:user db))]
     {:http-xhrio (-> {:format          (ajax/transit-request-format)
                       :response-format (ajax/transit-response-format)}
                      (merge request)
                      (cond-> token (assoc-in [:params :token] token))
                      (cond-> token (assoc-in [:headers const/session-header] token)))})))


(defn resolve-data
  "The resolve key is the top-level key for a page's state. The query and selection are
   used directly on the backend in a datahike pull query, and must form valid pull
   syntax."
  [{:keys [db]} [_ resolve-key query selection]]
  {:db (-> db
           (assoc-in [:resolve query :loading?] true)
           (assoc-in [resolve-key :query] query))
   :dispatch [::request {:method :post
                         :uri "/api/query"
                         :params {:query query :selection selection}
                         :on-success [::resolve-data-success resolve-key query]
                         :on-failure [::resolve-data-failure resolve-key query]}]})
(rf/reg-event-fx ::resolve-data [rf/debug] resolve-data)

(rf/reg-event-db
 ::resolve-data-success
 [rf/debug]
 (fn [db [_ resolve-key query response]]
   (assoc-in db [:resolve query] {:loading? false
                                  :data response})))

(rf/reg-event-db
 ::resolve-data-failure
 [rf/debug]
 (fn [db [_ resolve-key query response]]
   (assoc-in db [:resolve query] {:loading? false
                                  :data response})))

(rf/reg-sub
 ::resolve-data
 (fn [db [_ resolve-key]]
   (let [query (get-in db [resolve-key :query])]
     (get-in db [:resolve query :data]))))

(rf/reg-event-db
 ::clean-resolve
 [rf/debug]
 (fn [db [_ resolve-key]]
   (let [query (get-in db [resolve-key :query])]
     (-> db
         (dissoc resolve-key)
         (update :resolve dissoc query)))))

(rf/reg-fx
 ::set-local-storage
 (fn set-local-storage [[& keypairs]]
   (doseq [[key val] (partition 2 keypairs)]
     (.setItem js/localStorage key val))))

(rf/reg-fx
 ::remove-local-storage
 (fn remove-local-storage [[& keys]]
   (doseq [key keys]
     (.removeItem js/localStorage key))))

(rf/reg-fx
 ::add-window-event-listener
 (fn [[event-type fn]]
   (.addEventListener js/window event-type fn)))

(rf/reg-fx
 ::remove-window-event-listener
 (fn [[event-type fn]]
   (.removeEventListener js/window event-type fn)))

(rf/reg-event-fx
 ::dispatch*
 [rf/debug]
 (fn [_ [_ & dispatches]]
   {::dispatch-many dispatches}))

(rf/reg-fx
 ::dispatch-many
 (fn [dispatches]
   (doseq [event dispatches]
     (rf/dispatch event))))

(rf/reg-event-fx
 ::toggle-dark-mode
 [rf/debug]
 (fn [{:keys [db]} _]
   (let [new-status (not (:dark-mode db))]
     {:db (assoc db :dark-mode new-status)
      ::toggle-dark-mode! ["body"]
      ::set-local-storage ["dark-mode" new-status]})))

(rf/reg-fx
 ::toggle-dark-mode!
 (fn [[target]]
   (when-let [el (.getElementById js/document target)]
     (.toggle (.-classList el) "dark-mode"))))

(rf/reg-sub
 ::route-name
 (fn [_ _]
   (rf/subscribe [:kee-frame/route]))
 (fn [route _]
   (-> route :data :name)))

(rf/reg-event-fx
 ::start-ws
 [rf/debug]
 (fn [{:keys [db]} _]
   (let [token (:token (:user db))]
     {::websocket/open
      {:path "/ws"
       :dispatch ::handle-ws-msg
       :wrap-message (fn [msg] (-> msg (cond-> token (assoc :token token))))}})))

(rf/reg-event-fx
 ::handle-ws-msg
 [rf/debug]
 (fn [{:keys [db]} [_ msg]]
   (.log js/console msg)
   {:db db
    :dispatch (or (:dispatch (:message msg))
                  [:noop])}))

(rf/reg-event-fx
 ::send-ws-msg
 [rf/debug]
 (fn [{:keys [db]} [_ msg]]
   {:dispatch [::websocket/send "/ws" msg]}))

(rf/reg-sub
 ::user
 (fn [db _]
   (:user db)))

(rf/reg-sub
 ::token
 (fn [db _]
   (:token (:user db))))

(rf/reg-sub
 ::username
 (fn [db _]
   (:username (:user db))))

(rf/reg-sub
 ::user-id
 (fn [db _]
   (:id (:user db))))

(rf/reg-event-db
 ::noop
 (fn [db _]
   db))
