(ns idle.heist.game
  (:require [clojure.spec.alpha :as s]
            [idle.heist.data :as data]))

(s/fdef shift-bear
  :args (s/cat :bear ::data/bear)
  :ret ::data/bear)
(defn shift-bear [bear]
  (-> bear
      (update :bear dec)
      (update :criminal inc)))


(defn shift-criminal [bear]
  (-> bear
      (update :criminal dec)
      (update :bear inc)))
