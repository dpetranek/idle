(ns idle.heist.data
  (:require [clojure.spec.alpha :as s]))

(def bear-descriptors [:rookie :washed-up :retired :unhinged :slick :incompetent])

(def bear-type {:grizzly :terrify
                :polar :swim
                :panda :eat-anything-that-looks-like-bamboo
                :black :climb
                :sun :sense-honey
                :honey-badger :carnage})

(def roles [:muscle :brains :driver :hacker :thief :face])

(def hats [:trilby :top :bowler :flat-cap :cowboy :fez :crown])

(def con-organizer [:cunning+sly :greedy+wicked :clueless+exploitable :too-obsessed-with-honey
                    :spoilt-trust-fund-kid :ruthless+corrupt])

(def location-descr [:creepy :busy :run-down :beautiful :dangerous :lavish])
(def location [:lakeside-camp :fishing-village :metropolitan-city
             :convention-center :truck-convoy :wilderness-retreat])

(def prizes [:ultradense-megahoney
             :pure-manuka-extract
             :queen-of-all-bees
             :black-orchid-honey
             :haunted-lincoln-beehive
             :miss-universe-2017])

(def twists [:rigged-to-blow
             :cops-en-route
             :rival-bear-team
             :been-set-up
             :prize-is-fake
             :bees-are-angry])

(def security [:armed-guards
               :electrically-locked-doors
               :laser-tripwire-grids
               :cctv-network
               :impenetrable-vault
               :poison-gas])

(s/def :bear/id uuid?)
(s/def :bear/descriptor (set bear-descriptors))
(s/def :bear/type (set (map first bear-type)))
(s/def :bear/skill (set (map second bear-type)))
(s/def :bear/role (set roles))
(s/def ::hat (set hats))
(s/def :bear/hats (s/coll-of ::hat))
(s/def ::stat (s/int-in 0 7))
(s/def :bear/bear ::stat)
(s/def :bear/criminal ::stat)
(s/def :bear/name string?)
(s/def :bear/heist uuid?)

(s/def ::bear (s/and (s/keys :req [:bear/type :bear/descriptor :bear/skill :bear/role :bear/bear :bear/criminal
                                   :bear/heist]
                             :opt [:bear/name :bear/hats ])
                     #(= (+ (:bear/bear %) (:bear/criminal %))
                         6)))

(s/def :heist/id uuid?)
(s/def :heist/location (set location))
(s/def :heist/location-descr (set location-descr))
(s/def :heist/organizer (set con-organizer))
(s/def :heist/prize (set prizes))
(s/def :heist/security (s/coll-of (set security)))
(s/def :heist/twist (set twists))

(s/def ::heist (s/keys :req [:heist/location :heist/location-descr :heist/organizer
                             :heist/prize :heist/security :heist/twist]))

(defn choose-hats []
  (let [hat-roller (conj hats :roll-twice)
        result (rand-nth hat-roller)]
    (if (= result :roll-twice)
      [(rand-nth hats) (rand-nth hats)]
      [result])))

(defn generate-bear
  ([]
   (generate-bear nil))
  ([name]
   (generate-bear name {}))
  ([name opts]
   (let [[type skill] (rand-nth (seq bear-type))]
     (cond-> {:bear/descriptor (rand-nth bear-descriptors)
              :bear/type type
              :bear/skill skill
              :bear/role (rand-nth roles)
              :bear/bear 3
              :bear/criminal 3}
       name (assoc :bear/name name)
       (not (:hatless opts)) (assoc :bear/hats (choose-hats))))))

(comment
  (s/conform ::bear (assoc (generate-bear) :bear/heist #uuid "3e3b9b10-be4e-4e0f-93b3-7df2f2f484c2"))
  #:bear{:descriptor :incompetent, :type :grizzly, :skill :terrify, :role :face, :bear 3, :criminal 3, :hats [:bowler], :heist #uuid "3e3b9b10-be4e-4e0f-93b3-7df2f2f484c2"}
  )

(defn generate-game []
  {:heist/organizer (rand-nth con-organizer)
   :heist/location (rand-nth location)
   :heist/location-descr (rand-nth location-descr)
   :heist/prize (rand-nth prizes)
   :heist/twist (rand-nth twists)
   :heist/security [(rand-nth security) (rand-nth security)]})

(comment
  (generate-bear)
  {:descriptor :rookie, :type :polar, :skill :swim, :role :hacer, :bear 3, :criminal 3}
  (generate-bear "Randy")
  {:descriptor :slick, :type :honey-badger, :skill :carnage, :role :brains, :bear 3, :criminal 3}
  (generate-bear "Randy" {:hat true})

  (s/conform ::heist  (generate-game))
  #:heist{:organizer :ruthless+corrupt, :location :fishing-village, :location-descr :dangerous, :prize :black-orchid-honey, :twist :bees-are-angry, :security [:laser-tripwire-grids :armed-guards]}

  :clojure.spec.alpha/invalid

  (reduce (fn [game player] (assoc game (keyword (str "pid" player)) (generate-bear)))
          (generate-game)
          (range 4))
  {:location-descr :lavish,
   :prize :miss-universe-2017,
   :twist :rival-bear-team,
   :security [:poison-gas :poison-gas],
   :location :lakeside-camp,
   :organizer :too-obsessed-with-honey
   :pid3 {:descriptor :incompetent,
          :type :panda,
          :skill :eat-anything-that-looks-like-bamboo,
          :role :muscle,
          :bear 3,
          :criminal 3,
          :hats [:cowboy]},
   :pid0 {:descriptor :unhinged,
          :type :polar,
          :skill :swim,
          :role :muscle,
          :bear 3,
          :criminal 3,
          :hats [:fez]},
   :pid2 {:descriptor :washed-up,
          :type :black,
          :skill :climb,
          :role :muscle,
          :bear 3,
          :criminal 3,
          :hats [:cowboy]},
   :pid1 {:descriptor :washed-up,
          :type :grizzly,
          :skill :terrify,
          :role :muscle,
          :bear 3,
          :criminal 3,
          :hats [:top]}}



  )
