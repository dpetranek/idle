(ns idle.heist.views.join
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [idle.common :as common]
            [idle.forms :as form]
            [idle.heist.data :as data]
            [idle.heist.views.shared :as shared]))

(def select-bear-form-id ::select-bear-form)

(kf/reg-controller
 :route.heist/join
 {:params (fn [route] (when (= (-> route :data :name) :route.heist/join)
                        (:game-id (:path-params route))))
  :start (fn [_ game-id] [::common/dispatch*
                          [::common/resolve-data :route.heist/join [:heist/id (uuid game-id)] ["*"]]
                          [:form/register-form {:form-id ::select-bear-form
                                                :cmd :cmd/join-game
                                                :fields {:selected :bear1
                                                         :bear/heist (uuid game-id)
                                                         :bear1 (data/generate-bear)
                                                         :bear2 (data/generate-bear)}
                                                :handlers {:payload ::select-bear-payload
                                                           :success ::select-bear-success}}]])
  :stop [::common/dispatch*
         [::common/clean-resolve :route.heist/join]
         [:form/unregister-form select-bear-form-id]]})

(rf/reg-event-fx
 ::select-bear-payload
 [rf/debug]
 (fn [{:keys [db]} [_ form-id]]
   (let [{:keys [selected bear1 bear2 bear/heist]} (get-in db [form-id :values])]
     {:db (-> db
              (assoc-in [form-id :payload] (if (= selected :bear1) bear1 bear2))
              (assoc-in [form-id :payload :bear/heist] heist))
      :dispatch [:form/submit form-id]})))

(rf/reg-event-fx
   ::select-bear-success
   [rf/debug]
   (fn [{:keys [db]} [_ {:keys [id] :as resp}]]
     (let [game-id (get-in db [:resolve (get-in db [:heist :query]) :data :heist/id])]
       {:db db
        :navigate-to [:route.heist/play {:game-id game-id}]})))


(defn join-page []
  (let [heist (rf/subscribe [::common/resolve-data :route.heist/join])
        bear1 (rf/subscribe [:form/field-value select-bear-form-id :bear1])
        bear2 (rf/subscribe [:form/field-value select-bear-form-id :bear2])]
    (fn []
      [:div
       [:h2 "Honey Heist"]
       [:h3 "The target"]
       [shared/draw-scenario @heist]

       [:h3 "Join the crew"]
       [:form {:on-submit #(do (.preventDefault %)
                               (rf/dispatch [:form/start-submit select-bear-form-id]))}
        [:div {:style {:flex-direction :row :justify-content :space-around}}
         [:label.bear {:for :bear1}
          [:h4 {:style {:text-align :center}} "Bear 1"]
          [shared/bear-description @bear1]
          [form/field {:id :bear1 :name :selected
                       :type :radio :value :bear1
                       :form-id select-bear-form-id}]]

         [:label.bear {:for :bear2}
          [:h4 {:style {:text-align :center}} "Bear 2"]
          [shared/bear-description @bear2]
          [form/field {:id :bear2 :name :selected
                       :type :radio :value :bear2
                       :form-id select-bear-form-id}]]]
        [:button {:type :submit} "Heist!"]]])))
