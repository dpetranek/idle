(ns idle.heist.views.manage
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [idle.common :as common]
            [idle.heist.views.shared :as shared]))

(kf/reg-controller
 :route.heist/manage
 {:params (fn [route] (when (= (-> route :data :name) :route.heist/manage)
                        (:game-id (:path-params route))))
  :start (fn [_ game-id] [::common/resolve-data :route.heist/manage
                          [:heist/id (uuid game-id)]
                          ["*" {:heist/manager ["*"]} {:heist/bear ["*" {:bear/player ["*"]}]}]])
  :stop [::common/clean-resolve :route.heist/manage]})

(defn manage-page []
  (let [heist (rf/subscribe [::common/resolve-data :route.heist/manage])]
    (fn []
      [:div
       [:h2 "Honey Heist"]
       [:h3 "Manage heist"]
       [shared/draw-scenario @heist]
       [:h4 "Assemble a crew"]
       (let [{:heist/keys [bear]} @heist]
         [:div.bears
          (for [criminal bear] ^{:key (:bear/id criminal)}
            [shared/bear-view criminal])])
       [:input {:type :text :read-only true :value (kf/path-for [:route.heist/join {:game-id (:heist/id @heist)}])}]
       [:a {:href (kf/path-for [:route.heist/play {:game-id (:heist/id @heist)}])} "Roll out"]])))
