(ns idle.heist.views.play
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [idle.common :as common]
            [idle.heist.views.shared :as shared]))

(kf/reg-controller
 :route.heist/play
 {:params (fn [route] (when (= (-> route :data :name) :route.heist/play)
                        (:game-id (:path-params route))))
  :start (fn [_ game-id] [::common/resolve-data :route.heist/play
                          [:heist/id (uuid game-id)]
                          ["*" {:heist/manager ["*"]} {:heist/bear ["*" {:bear/player ["*"]}]}]])
  :stop [::common/clean-resolve :route.heist/play]})

(defn play-page []
  (let [heist (rf/subscribe [::common/resolve-data :route.heist/play])
        user-id (rf/subscribe [::common/user-id])
        route-params (rf/subscribe [:kee-frame/route])]
    (fn []
      (let [{:heist/keys [bear]} @heist
            user-id @user-id
            game-id (:game-id (:path-params @route-params))]
        [:div.heist
         [:h3 "HONEYCON 2020"]
         [:a {:href (kf/path-for [:route.heist/manage {:game-id game-id}])} "Manage"]
         [shared/draw-scenario @heist]
         [:div.bears
          (for [criminal bear]
            (let [{{id :user/id} :bear/player} criminal]
              (if (= id user-id)
                ^{:key (:bear/id criminal)} [shared/bear-active criminal]
                ^{:key (:bear/id criminal)} [shared/bear-view criminal])))]]))))
