(ns idle.heist.views.start
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [idle.common :as common]
            [idle.forms :as form]
            [idle.heist.data :as data]
            [clojure.string :as str]))

(def heist-start-form-id ::heist-start-form)

(kf/reg-controller
 :route.heist/start
 {:params (fn [route] (when (= (-> route :data :name) :route.heist/start) true))
  :start [:form/register-form {:form-id heist-start-form-id
                               :cmd :cmd/create-heist
                               :fields {:selected :scenario1
                                        :scenario1 (data/generate-game)
                                        :scenario2 (data/generate-game)}
                               :handlers {:payload ::start-heist-payload
                                          :success ::start-heist-success}}]
  :stop [:form/unregister-form heist-start-form-id]})

(rf/reg-event-fx
 ::start-heist-payload
 [rf/debug]
 (fn [{:keys [db]} [_ form-id]]
   (let [{:keys [selected scenario1 scenario2]} (get-in db [form-id :values])]
     {:db (assoc-in db [form-id :payload] (if (= selected :scenario1)
                                            scenario1
                                            scenario2))
      :dispatch [:form/submit form-id]})))

(rf/reg-event-fx
   ::start-heist-success
   [rf/debug]
   (fn [{:keys [db]} [_ {:keys [id] :as resp}]]
     {:db db
      :navigate-to [:route.heist/manage {:game-id id}]}))

(defn start-page []
  (let [scenario1 (rf/subscribe [:form/field-value heist-start-form-id :scenario1])
        scenario2 (rf/subscribe [:form/field-value heist-start-form-id :scenario2])
        selected  (rf/subscribe [:form/field-value heist-start-form-id :selected])]
    (fn []
      [:div
       [:h2 "Honey Heist"]
       [:h3 "Pick a target"]
       [:form {:on-submit #(do (.preventDefault %)
                               (rf/dispatch [:form/start-submit heist-start-form-id]))}
        [:div {:style {:flex-direction :row :justify-content :space-around}}
         [:label.scenario {:for :scenario1}
          [:h4 {:style {:text-align :center}} "Scenario 1"]
          [:ul
           (for [[key val] (sort @scenario1)] ^{:key (str key "1")}
             [:li [:span (str (name key) ": ")] [:strong (if (coll? val)
                                                           (str/join ", " (map name val))
                                                           val)]])]
          [form/field {:id :scenario1 :name :selected
                       :type :radio :value :scenario1
                       :form-id heist-start-form-id }]]
         [:label.scenario {:for :scenario2}
          [:h4 {:style {:text-align :center}} "Scenario 2"]
          [:ul
           (for [[key val] (sort @scenario2)] ^{:key (str key "2")}
             [:li [:span (str (name key) ": ")] [:strong (if (coll? val)
                                                           (str/join ", " (map name val))
                                                           val)]])]
          [form/field {:id :scenario2 :name :selected
                       :type :radio  :value :scenario2
                       :form-id heist-start-form-id }]]]
        [:button {:type :submit} "Next"]]])))
