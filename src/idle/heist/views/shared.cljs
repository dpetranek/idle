(ns idle.heist.views.shared
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [idle.locale :as i18n]
            [idle.common :as common]))

(rf/reg-event-fx
 ::shift-bear
 [rf/debug]
 (fn [{:keys [db]} [_ bear-id]]
   {:dispatch [::common/send-ws-msg {:cmd :cmd/shift-bear
                                     :payload {:bear/id bear-id}}]}))

(rf/reg-event-fx
 ::shift-criminal
 [rf/debug]
 (fn [{:keys [db]} [_ bear-id]]
   {:dispatch [::common/send-ws-msg {:cmd :cmd/shift-criminal
                                     :payload {:bear/id bear-id}}]}))

(rf/reg-event-fx
 ::shift-stat
 [rf/debug]
 (fn [{:keys [db]} [_ bear-id stat]]
   {:dispatch [::common/send-ws-msg {:cmd :cmd/shift-stat
                                     :payload {:stat stat
                                               :bear/id bear-id}}]}))

(defn draw-scenario [heist]
  (let [{:heist/keys [location location-descr organizer prize security]} heist]
    [:div.scenario
     [:div.location (str location-descr " " location)]
     [:div.organizer organizer]
     [:div.prize prize]
     [:div.security
      [:ul
       (for [measure security] ^{:key measure}
         [:li measure])]]]))

(defn bear-description [bear]
  (let [{:bear/keys [type skill role descriptor name player id]} bear]
    (when bear
      [:div.bear {:style {:max-width "20em" }}
       [:span (:user/username player)]
       [:div.portrait {:style {:height "150px" :border "thick solid black"
                               :align-items :center :justify-content :center}}
        [:div.head {:style {:width "50px" :height "50px" :border-radius "50%" :background-color "black"
                            :padding-bottom "1em"}}]
        (str (i18n/t [(keyword :heist.bear.descriptor descriptor)]) " "
             (i18n/t [(keyword :heist.bear.type type)]))]
       [:span {:style {:flex "auto"}} "Skill: " (i18n/t [(keyword :heist.bear.skill skill)])]
       [:span {:style {:flex "auto"}} "Role: " (i18n/t [(keyword :heist.bear.role role)]) ]])))

(defn draw-stat [stat]
  [:div.stat-meter {:style {:flex "auto" :width "1em" }}
   stat
   [:div.stat {:style {:height (str (- 100 (* (/ stat 6) 100)) "%")}}]
   [:div.stat {:style {:height (str (* (/ stat 6) 100) "%") :background-color "black"}}]])

(defn bear-view [bear]
  (let [{:bear/keys [type skill role descriptor name player id]
         bear-stat :bear/bear criminal-stat :bear/criminal} bear]
    (when bear
      [:div.bear {:style {:max-width "20em"}}
       [:span (:user/username player)]
       [:div.portrait {:style {:height "150px" :border "thick solid black"
                               :align-items :center :justify-content :center}}
        [:div.head {:style {:width "50px" :height "50px" :border-radius "50%" :background-color "black"
                            :padding-bottom "1em"}}]
        (str (i18n/t [(keyword :heist.bear.descriptor descriptor)]) " "
             (i18n/t [(keyword :heist.bear.type type)]))]
       [:span {:style {:flex "auto"}} "Skill: " (i18n/t [(keyword :heist.bear.skill skill)])]
       [:span {:style {:flex "auto"}} "Role: " (i18n/t [(keyword :heist.bear.role role)])]
       [:div.row {:style {:justify-content "space-around"}}
        [:div {:style {:flex "auto" :align-items "flex-end" :margin "0 0.5rem"}}
         [draw-stat bear-stat]
         [:span "Bear"]]
        [:div {:style {:flex "auto" :align-items "flex-start"}}
         [draw-stat criminal-stat]
         [:span "Criminal"]]]])))

(defn bear-active [bear]
  (let [{:bear/keys [type skill role heist hats descriptor criminal name id player]
         bear-stat :bear/bear criminal-stat :bear/criminal} bear]
    (when bear
      [:div.bear {:style {:max-width "20em"}}
       [:span (:user/username player)]
       [:div.portrait {:style {:height "150px" :border "thick solid black"
                               :align-items :center :justify-content :center}}
        [:div.head {:style {:width "50px" :height "50px" :border-radius :background-color "50%" "black"
                            :padding-bottom "1em"}}]
        (i18n/t [(keyword :heist.bear.descriptor descriptor)]) " "
        (i18n/t [(keyword :heist.bear.type type)])]
       [:span {:style {:flex "auto"}} "Skill: " (i18n/t [(keyword :heist.bear.skill skill)])]
       [:span {:style {:flex "auto"}} "Role: " (i18n/t [(keyword :heist.bear.role role)])]
       [:div.row {:style {:justify-content "space-around"}}
        [:div {:style {:flex "auto" :align-items "flex-end" :margin "0 0.5rem"}}
         [draw-stat bear-stat]
         [:span "Bear"]
         (when (< 0 bear-stat 6)
           [:button {:title "shift bear" :on-click #(rf/dispatch [::shift-stat id :bear])} "<"])]
        [:div {:style {:flex "auto" :align-items "flex-start"}}
         [draw-stat criminal-stat]
         [:span "Criminal"]
         (when (< 0 criminal-stat 6)
           [:button {:title "shift criminal" :on-click #(rf/dispatch [::shift-stat id :criminal])} ">"])]]
       ])))
