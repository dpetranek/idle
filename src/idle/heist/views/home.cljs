(ns idle.heist.views.home
  (:require [kee-frame.core :as kf]))

(defn honey-page []
  [:div [:h2 "Honey Heist"]
   [:p "It's HONEYCON 2020. You are going to undertake the greatest heist the world has ever seen."]
   [:p "Two Things:"]
   [:ol
    [:li "You have a complex plan that requires precise timing."]
    [:li "You are GODDAMN BEAR."]]
   [:a {:href (kf/path-for [:route.heist/start])} "Start a heist"]
   ])
