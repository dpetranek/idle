(ns idle.client
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]
            [clojure.edn :as edn]
            [idle.forms]
            [idle.home.events]
            [idle.common :as common]
            [idle.home.views.home :as home]
            [idle.home.views.layout :as layout]
            [idle.home.views.register :as register]
            [idle.home.views.reset :as reset]
            [idle.user.views.home :as user.home]
            [idle.boggle.views.home :as boggle.home]
            [idle.boggle.views.play :as boggle.play]
            [idle.masks.views.home :as masks.home]
            [idle.heist.views.home :as heist.home]
            [idle.heist.views.start :as heist.start]
            [idle.heist.views.join :as heist.join]
            [idle.heist.views.manage :as heist.manage]
            [idle.heist.views.play :as heist.play]))

(defn app []
  [:div.app
   [:main
    [kf/switch-route (fn [route] (-> route :data :name))
     :route/home         [home/home-page]
     :route/register     [register/register-page]
     :route/reset        [reset/reset-page]
     :route.user/home    [user.home/user-page]
     :route.boggle/home  [boggle.home/boggle-page]
     :route.boggle/play  [boggle.play/play-page]
     :route.heist/home   [heist.home/honey-page]
     :route.heist/start  [heist.start/start-page]
     :route.heist/manage [heist.manage/manage-page]
     :route.heist/join   [heist.join/join-page]
     :route.heist/play   [heist.play/play-page]
     :route.masks/home   [masks.home/masks-page]
     nil                 [:div "route not found"]]]
   [layout/sidebar]
   [layout/footer]]
  )

(defn initialize []
  (let [user (edn/read-string (.getItem js/localStorage "user"))
        dark-mode (edn/read-string (.getItem js/localStorage "dark-mode"))]
    (when-let [el (.getElementById js/document "body")]
      (when dark-mode
        (.toggle (.-classList el) "dark-mode")))
    (rf/dispatch [::common/start-ws])
    (-> {}
        (cond-> user (assoc :user user))
        (cond-> dark-mode (assoc :dark-mode dark-mode)))))

(defn main!
  []
  (kf/start! {:routes [["/" :route/home]
                       ["/register" :route/register]
                       ["/reset" :route/reset]
                       ["/boggle" :route.boggle/home]
                       ["/boggle/:board" :route.boggle/play]
                       ["/honey-heist" :route.heist/home]
                       ["/honey-heist/start" :route.heist/start]
                       ["/honey-heist/:game-id/play" :route.heist/play]
                       ["/honey-heist/:game-id/join" :route.heist/join]
                       ["/honey-heist/:game-id/manage" :route.heist/manage]
                       ["/u/:user-id" :route.user/home]
                       ["/masks" :route.masks/home]]
              :initial-db (initialize)
              :root-component [app]})
  (println "idle loaded"))
